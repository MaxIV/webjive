Attribute Logger
****************

The widget shows a value coming from an attribute in a log format with or without the timestamp. 

Widget setting 
===============

The user has the ability to:

- Attribute to log: select the device and the attribute to log
- Show Last Value: user can select whether to see title displayed or not
- Lines Logged: set the number of lines logged 
- Show device: if selected, show the device name
- Attribute Display: select if show the name, the label of the device or None. Label is shown as default
- Show timestamp column, true or false to either display timestamp of values or not
- OuterDiv CSS: Advanced user can aplly custom CSS to outer div on the widget
- Last Value CSS: Advanced user can aplly custom CSS to top header showing last value
- Table CSS: Advanced user can aplly custom CSS to table

\ |IMG1|\ 

Widget design 
===============

\ |IMG2|\ 



.. bottom of content

.. |IMG1| image:: _static/img/attribute_logger_inspector.png
   :height: 453 px
   :width: 291 px

.. |IMG2| image:: _static/img/attribute_logger_view.png
   :height: 287 px
   :width: 464 px