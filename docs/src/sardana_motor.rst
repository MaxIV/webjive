Sardana Motor
**************

The widget allows a user to control Sardana motor from Taranta. It provides a graphical interface to move a motor to a specified position with monitoring its state transition.

Widget setting 
===============

Two parameters are required to be configured for this widget. 

+---------------+---------------------------------------------------------+
|Device         |Description                                              |
+===============+=========================================================+
|Device         |Specify the motor device to be running                   |
+---------------+---------------------------------------------------------+
|Precision      |Specify the precision when showing the current position  |                   
+---------------+---------------------------------------------------------+


An example of the widget inspector in Edit mode.

\ |IMG1|\

In run mode
===============
The motor state and it's current position will be shown in the running mode. You can specify a target position value in the input box, and choose either absolute or relative movement by highlighting the triangle box. \
Click button **Move** to move the motor. When it starts moving, the motor state will switch to Running. It takes a while to move to the position, which depends on the motor velocity and the distance. \
After it reaches the target position, the state returns to On.


\ |IMG2|\


.. bottom of content

.. |IMG1| image:: _static/img/sardana_motor_inspector.png
   :height: 170 px
   :width: 297 px


.. |IMG2| image:: _static/img/sardana_motor_running.png
   :height: 50 px
   :width: 416 px