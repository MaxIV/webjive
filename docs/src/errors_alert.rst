Error Handling Documentation
============================

Overview
--------
This document describes the error handling mechanism implemented in the web application. The error handler is designed to catch runtime JavaScript errors that occur in the web application and provide a user-friendly interface for reporting these errors.

Implementation Details
----------------------
The global error handler is set up to capture any unhandled errors that occur during the execution of JavaScript code in the web application. The handler constructs a detailed error message, which is then displayed to the user via a graphical alert provided by `swal` (SweetAlert).

|IMG1|

Error Handler Code
------------------
The following JavaScript code snippet shows the implementation of the global error handler:

.. code-block:: text

    window.onerror = function (message, source, lineno, colno, error) {
        // Create a user-friendly error message
        const errorData = {
            message: message,
            file: source,
            line: lineno,
            column: colno,
            errorStack: error ? error.stack : 'No stack trace'
        };
        const errorMessage = `Oops! Something went wrong 😢\n\n` +
                             `Error: ${JSON.stringify(errorData)}\n` +
                             `This occurred at line ${lineno} of the source file`;
        // Show the error message in an swal alert
        swal(errorMessage, {
            buttons: ["Copy error to Clipboard", "Exit"],
        }).then((value) => {
            console.log(value);
            switch (value) {
                case null:
                    navigator.clipboard.writeText(JSON.stringify(errorData));
                    swal("Copied to clipboard, please inform taranta developers about this error");
                    break;
                default:
                    break;
            }
        });
    };

Error Alert Details
-------------------
Upon encountering an error, the user is presented with an alert dialog containing the following options:

1. **Copy error to Clipboard**: When selected, the error details are copied to the clipboard. This allows the user to easily share the error information with the development team for further analysis.
2. **Exit**: Allows the user to dismiss the alert without any additional actions.

Both buttons provide immediate feedback to the user. If the "Copy error to Clipboard" button is pressed, a second alert confirms that the error details have been copied and provides an instruction to contact the development team.

This error handling mechanism ensures that users are not left with an unresponsive application without feedback and have a clear path to report issues.


.. |IMG1| image:: _static/img/Error.gif
   :width: 530 px
