SVG Validation
==============

Validate Layers
---------------

The `validateLayers` function is designed to validate layer definitions within SVG elements. It ensures that specified layers exist in the provided SVG content and that their associated conditions are properly formatted and logically consistent.

Function Details
----------------

1. **Initialization**

   - Initializes an empty `errors` array to collect validation errors.
   - Defines a set of allowed operators and a regular expression to validate condition expressions.

2. **SVG Normalization**

   - Removes all newline characters from `svgString` to facilitate substring searches.

3. **Layer Filtering**

   - Extracts all rules of type `"layer"` from the `elements.rules` array.

4. **Layer Existence Validation**

   - For each layer rule, checks if the corresponding layer exists in the SVG by searching for the specific `inkscape:label` attribute.
   - If the layer is not found, an error is added to the `errors` array.

5. **Condition Validation**

   - Ensures that each condition is not empty.
   - Validates that the condition contains only allowed characters.
   - Checks that the condition includes at least one allowed operator.
   - Verifies that the condition references the variable `value`.
   - Evaluates the condition against a set of test values to ensure it is neither always true nor always false.
   - Catches and reports any errors encountered during condition evaluation.

Error Handling
--------------

All validation errors are collected in the `errors` array and returned at the end of the function execution. Each error provides context about which rule failed and why.

Example Usage
-------------

To use the `validateLayers` function, you need to provide it with an `elements` object containing layer rules and an `svgString` representing your SVG content. The function will return an array of errors if any validations fail.

Error Examples
--------------

Here are some examples of possible errors that `validateLayers` might return:

- **Layer Not Found**

  Indicates that a specified layer does not exist in the provided SVG content.

- **Invalid Characters in Condition**

  The condition contains characters that are not permitted, potentially leading to syntax errors or unintended behavior.

- **Condition Always True**

  The condition evaluates to true for all tested values, which might indicate a logical error in the condition.

- **Condition Does Not Reference 'value'**

  The condition does not reference the expected `value` variable, making it ineffective for dynamic evaluations.

These errors help developers quickly identify and correct issues with layer definitions and their associated conditions within SVG files.