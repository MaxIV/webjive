.. _link-devices-on-svg:

Link SVG element to tango device and attributes
***********************************************

After creating SVG, each of its elements can be associated to a tango device or device attribute.
**To associate an SVG element to a tango device right click SVG element > Object Properties(Ctrl+shift+O),
write the name of tango device under description section and click set button** in the bottom to save 
the changes as shown below:

\ |IMG1|\ 


In above image user is trying to link ``sys/tg_test/1`` device to an SVG element. 
If no attribute is present along with device name then by default device **status** attribute will be linked
The syntax is ``model=<tango_device_name>/<attribute_name>``
For ex. to link **short_scalar** attribute from **sys/tg_test/1** the description text would
be ``model=sys/tg_test/1/short_scalar`` as shown below:

\ |IMG2|\ 

Similarly user can link any device attribute (scalar / spectrum) to an SVG element. For spectrum user need to use
his skill to show it correctly on the widget.

**One SVG element should be linked to only one tango device / attribute.**

Once the linking task is done, user can export the file as \*.svg from InkSpace. And this exported svg file
can be used in the SVG Widget.


.. bottom of content

.. |IMG1| image:: _static/img/link-device.png

.. |IMG2| image:: _static/img/link_device_1.png
