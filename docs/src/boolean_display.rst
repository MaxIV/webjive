Boolean Display Widget
***********************

The **Boolean Display Widget** enables the user to set boolean values by using a switch button.

Widget setting 
===============

\ |IMG1|\ 

* Device Name: Show or not device name (boolean)
* Align switch right: Either have switch align to the right or not
* Custom CSS: CSS to be applied to the outer div of the widget
* On State CSS: CSS to be applied to the true state of the switch
* Off State CSS: CSS to be applied to the false state of the switch

Widget design 
===============

\ |IMG2|\ 

Applying CSS to widget
======================

One can also write css under ``custom css`` section. The css syntax is same as we write for html files

\ |IMG3|\ 

The CSS rules written in ``custom css`` section are applied to the widget as below:

\ |IMG4|\ 


.. bottom of content

.. |IMG1| image:: _static/img/boolean_display_widget_settings.png
   :height: 617 px
   :width: 293 px

.. |IMG2| image:: _static/img/boolean_display_widget.png
   :height: 44 px
   :width: 282 px

.. |IMG3| image:: _static/img/boolean_display_css_setting.png
   :height: 381 px
   :width: 293 px

.. |IMG4| image:: _static/img/boolean_display_css_output.png
   :height: 58 px
   :width: 277 px
