Configuring Environment
***********************


Overview
========

This documentation provides guidelines for sharing a dashboard across various deployment / namespaces
using ``environment`` field. This environment field can be set from Dashboard Settings > Environment tab

The environment field would be helpful in sharing dashboard across different deployment / namespaces.
Depending upon the env


How to use environment field
============================
Before setting up the environment field, identify to which deployments the dashboard needs to be shared with.
If you want to continue with exiting behavior i.e dashboard would be visible to current deployment simple copy url and paste it in the environment field.

\ |IMG1|\ 

\ |IMG2|\ 

Hit ``Enter`` key to add a value to environment. A dashboard can have multiple value for env. For eg a 
dashboard can be shared with specific deployments on a cluster. Finally hit ``save`` button to persist the value in database.

There are 3 levels across which one can share a dashboard

.. list-table:: 
   :widths: 1 2
   :header-rows: 1

   * - Environment value
     - Dashboard shared areas
   * - https://k8s.stfc.skao.int/taranta-namespace/taranta/
     - Dashboard would be visible to all users in ``taranta-namespace`` deployment
   * - https://k8s.stfc.skao.int/*
     - Dashboard would be visible to all the namespaces deployed under ``https://k8s.stfc.skao.int``
   * - /\*/\*/\*
     - Dashboard would be shared with every one on all infrasture and deployments

Unique Dashboard Naming by Environment
======================================

Each dashboard name must be unique within its specific environment. 
This uniqueness allows users to have the same dashboard name across different 
environments without conflict. For example, a dashboard named "System Status" 
can exist in both "dev" and "prod" environments as separate entities.

When a dashboard is assigned to an environment where a dashboard with the same name already exists, 
the system will automatically append a copy identifier, such as "copy-X", to its name. This ensures 
that each dashboard within an environment maintains a unique identity. The number "X" represents a 
sequential number starting from 1, incrementing for each additional duplicate name.

For instance:

- If "System Status" is created in the "dev" environment and a new dashboard with the same name is also assigned to the "dev" environment, the second dashboard will be renamed to "System Status - copy-1".
- If another "System Status" dashboard is introduced to the "dev" environment, it will be named "System Status - copy-2", and so on.

This naming convention helps users distinguish between different iterations of dashboards that 
are shared across multiple environments.

.. note::
   The automatic renaming will only occur if the dashboard is shared with an environment where a 
   dashboard of the same name already exists. If the dashboard is exclusive to a single environment, 
   its name will remain as originally set by the user.

.. bottom of content

.. |IMG1| image:: _static/img/config_env.png

.. |IMG2| image:: _static/img/env_setting.png
