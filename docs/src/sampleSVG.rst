Sample SVG with Format & Rules
==============================

To use **ENUM** (test/tarantatestdevice/1/dishstate) values in an rule as below, the svg element with label layer5(this could be a layer or any SVG element) will show when the **dishstate** value is Slew.

.. code-block:: text

    rule5: {
        type=layer
        model=test/tarantatestdevice/1/dishstate
        layer=layer5
        condition=value==Slew
    }


Similarly we can have rule condition for attriute(short_scalar) with numerical values:

.. code-block:: text

    rule5: {
        type=layer
        model=sys/tg_test/1/short_scalar
        layer=layer4
        condition=(value>40 and value<60)
    }


The below sample SVG demonstrates the usage of rules and formattings in SVG widget & synoptics. User can create as many rules with different types of attributes.

.. code-block:: html

    <?xml version="1.0" encoding="UTF-8" standalone="no"?>
    <!-- Created with Inkscape (http://www.inkscape.org/) -->

    <svg
        width="1200"
        height="1000"
        viewBox="0 0 317.49999 264.58333"
        version="1.1"
        id="svg1"
        inkscape:version="1.3.2 (091e20e, 2023-11-25, custom)"
        sodipodi:docname="layer.svg"
        xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
        xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
        xmlns="http://www.w3.org/2000/svg"
        xmlns:svg="http://www.w3.org/2000/svg">
        <sodipodi:namedview
            id="namedview1"
            pagecolor="#ffffff"
            bordercolor="#000000"
            borderopacity="0.25"
            inkscape:showpageshadow="2"
            inkscape:pageopacity="0.0"
            inkscape:pagecheckerboard="0"
            inkscape:deskcolor="#d1d1d1"
            inkscape:document-units="px"
            inkscape:zoom="0.5052024"
            inkscape:cx="363.22076"
            inkscape:cy="494.85117"
            inkscape:window-width="1920"
            inkscape:window-height="991"
            inkscape:window-x="-9"
            inkscape:window-y="-9"
            inkscape:window-maximized="1"
            inkscape:current-layer="main" />
        <defs
            id="defs1" />
        <g
            inkscape:label="Main"
            inkscape:groupmode="layer"
            id="main">
            <rect
            style="fill:#aaffaa;fill-opacity:0.646182;fill-rule:evenodd;stroke:#235213;stroke-width:0.470001;stroke-linejoin:bevel;paint-order:stroke fill markers"
            id="rect2"
            width="317.5"
            height="264.58334"
            x="0"
            y="0"
            inkscape:label="background" />
            <g
            inkscape:groupmode="layer"
            id="layer1"
            inkscape:label="Bottom Layer">
            <text
                xml:space="preserve"
                style="font-size:8.00401px;line-height:0px;font-family:sans-serif;-inkscape-font-specification:sans-serif;fill:#371f27;fill-opacity:0.646182;fill-rule:evenodd;stroke:#235213;stroke-width:0.333242;stroke-linejoin:bevel;stroke-dasharray:none;paint-order:stroke fill markers"
                x="15.80725"
                y="239.86795"
                id="text10-4-5"
                transform="scale(0.95389047,1.0483384)"><desc
                id="desc10-5-8">model=test/tarantatestdevice/1/routingtable</desc><tspan
                sodipodi:role="line"
                id="tspan10-3-64"
                style="stroke-width:0.333241"
                x="15.80725"
                y="239.86795">Routing Table</tspan></text>
            <text
                xml:space="preserve"
                style="font-size:8.00401px;line-height:0px;font-family:sans-serif;-inkscape-font-specification:sans-serif;fill:#371f27;fill-opacity:0.646182;fill-rule:evenodd;stroke:#235213;stroke-width:0.333242;stroke-linejoin:bevel;stroke-dasharray:none;paint-order:stroke fill markers"
                x="86.10144"
                y="218.89832"
                id="text10-4-49-9"
                transform="scale(0.95389047,1.0483384)"
                inkscape:label="long_scalar_txt"><desc
                id="desc10-5-5-1">model=sys/tg_test/1/long_scalar
        format=concat(Long Scalar: )
        </desc><tspan
                sodipodi:role="line"
                id="tspan10-3-2-9"
                style="stroke-width:0.333241"
                x="86.10144"
                y="218.89832">Long Scalar</tspan></text>
            <text
                xml:space="preserve"
                style="font-size:8.00401px;line-height:0px;font-family:sans-serif;-inkscape-font-specification:sans-serif;fill:#371f27;fill-opacity:0.646182;fill-rule:evenodd;stroke:#235213;stroke-width:0.333242;stroke-linejoin:bevel;stroke-dasharray:none;paint-order:stroke fill markers"
                x="253.92024"
                y="218.89832"
                id="text10-4-49-9-0"
                transform="scale(0.95389047,1.0483384)"
                inkscape:label="dishtext"><desc
                id="desc10-5-5-1-7">model=testdb://test/tarantatestdevice/1/dishstate
        format=concat(Dish State: )
        </desc><tspan
                sodipodi:role="line"
                id="tspan10-3-2-9-7"
                style="stroke-width:0.333241"
                x="253.92024"
                y="218.89832">Dish State</tspan></text>
            <text
                xml:space="preserve"
                style="font-size:8.00401px;line-height:0px;font-family:sans-serif;-inkscape-font-specification:sans-serif;fill:#371f27;fill-opacity:0.646182;fill-rule:evenodd;stroke:#235213;stroke-width:0.333242;stroke-linejoin:bevel;stroke-dasharray:none;paint-order:stroke fill markers"
                x="15.803864"
                y="219.13867"
                id="text10-4-49-9-7"
                transform="scale(0.95389047,1.0483384)"><desc
                id="desc10-5-5-1-4">model=sys/tg_test/1/short_scalar
        format=concat(Short Scalar: )</desc><tspan
                sodipodi:role="line"
                id="tspan10-3-2-9-3"
                style="stroke-width:0.333241"
                x="15.803864"
                y="219.13867">short_scalar</tspan></text>
            </g>
            <g
            inkscape:groupmode="layer"
            id="layer10"
            inkscape:label="RightLayer">
            <text
                xml:space="preserve"
                style="font-size:8.00401px;line-height:0px;font-family:sans-serif;-inkscape-font-specification:sans-serif;fill:#371f27;fill-opacity:0.646182;fill-rule:evenodd;stroke:#235213;stroke-width:0.333242;stroke-linejoin:bevel;stroke-dasharray:none;paint-order:stroke fill markers"
                x="222.29459"
                y="129.67099"
                id="text10-4"
                transform="scale(0.95389047,1.0483384)"><desc
                id="desc10-5">model=sys/tg_test/1/long_spectrum_ro
        format=index(10)
        format=numeral(0.00)
        format=concat(Long Spectrum[10]: )
        </desc><tspan
                sodipodi:role="line"
                id="tspan10-3"
                style="stroke-width:0.333241"
                x="222.29459"
                y="129.67099">long_spectrum_ro</tspan></text>
            <text
                xml:space="preserve"
                style="font-size:8.00401px;line-height:0px;font-family:sans-serif;-inkscape-font-specification:sans-serif;fill:#371f27;fill-opacity:0.646182;fill-rule:evenodd;stroke:#235213;stroke-width:0.333242;stroke-linejoin:bevel;stroke-dasharray:none;paint-order:stroke fill markers"
                x="221.03256"
                y="160.52274"
                id="text10-4-4"
                transform="scale(0.95389047,1.0483384)"><desc
                id="desc10-5-6">model=sys/tg_test/1/float_spectrum_ro
        format=index(10)
        format=numeral(0.0)
        </desc><tspan
                sodipodi:role="line"
                id="tspan10-3-6"
                style="stroke-width:0.333241"
                x="221.03256"
                y="160.52274">float_spectrum_ro</tspan></text>
            <text
                xml:space="preserve"
                style="font-size:8.00401px;line-height:0px;font-family:sans-serif;-inkscape-font-specification:sans-serif;fill:#371f27;fill-opacity:0.646182;fill-rule:evenodd;stroke:#235213;stroke-width:0.333242;stroke-linejoin:bevel;stroke-dasharray:none;paint-order:stroke fill markers"
                x="222.24377"
                y="69.620682"
                id="text10-4-3"
                transform="scale(0.95389047,1.0483384)"><desc
                id="desc10-5-7">model=test/tarantatestdevice/1/processorinfo
        format=/temperature
        format=numeral(0)
        format=concat(temperature: )
        </desc><tspan
                sodipodi:role="line"
                id="tspan10-3-62"
                style="stroke-width:0.333241"
                x="222.24377"
                y="69.620682">Processor Info</tspan></text>
            <text
                xml:space="preserve"
                style="font-size:8.00401px;line-height:0px;font-family:sans-serif;-inkscape-font-specification:sans-serif;fill:#371f27;fill-opacity:0.646182;fill-rule:evenodd;stroke:#235213;stroke-width:0.333242;stroke-linejoin:bevel;stroke-dasharray:none;paint-order:stroke fill markers"
                x="221.73065"
                y="98.901321"
                id="text10-4-3-3"
                transform="scale(0.95389047,1.0483384)"><desc
                id="desc10-5-7-5">model=sys/tg_test/1/double_spectrum_ro
        format=substr(1,4)
        format=concat(DS[1-4]: )</desc><tspan
                sodipodi:role="line"
                id="tspan10-3-62-0"
                style="stroke-width:0.333241"
                x="221.73065"
                y="98.901321">double_spectrum_ro</tspan></text>
            <text
                xml:space="preserve"
                style="font-size:8.00401px;line-height:0px;font-family:sans-serif;-inkscape-font-specification:sans-serif;fill:#371f27;fill-opacity:0.646182;fill-rule:evenodd;stroke:#235213;stroke-width:0.333242;stroke-linejoin:bevel;stroke-dasharray:none;paint-order:stroke fill markers"
                x="222.11914"
                y="39.249649"
                id="text10-4-49"
                transform="scale(0.95389047,1.0483384)"><desc
                id="desc10-5-5">model=sys/tg_test/1/double_scalar
        format=numeral(0.00)
        format=concat(Double Scalar: )
        </desc><tspan
                sodipodi:role="line"
                id="tspan10-3-2"
                style="stroke-width:0.333241"
                x="222.11914"
                y="39.249649">double_scalar</tspan></text>
            </g>
            <g
            inkscape:groupmode="layer"
            id="layerr9"
            inkscape:label="layer5">
            <rect
                style="fill:#eed7f4;fill-opacity:1;fill-rule:evenodd;stroke:#235213;stroke-width:0.67513;stroke-linejoin:bevel;paint-order:stroke fill markers"
                id="rect1"
                width="108.29942"
                height="15.261945"
                x="15.6564"
                y="9.5885468" />
            <text
                xml:space="preserve"
                style="font-size:11.2889px;line-height:0px;font-family:sans-serif;-inkscape-font-specification:sans-serif;fill:#371f27;fill-opacity:0.646182;fill-rule:evenodd;stroke:#235213;stroke-width:0.470001;stroke-linejoin:bevel;stroke-dasharray:none;paint-order:stroke fill markers"
                x="45.970982"
                y="21.483887"
                id="text9"><tspan
                sodipodi:role="line"
                id="tspan9"
                style="stroke-width:0.47"
                x="45.970982"
                y="21.483887">Layer 5</tspan></text>
            </g>
            <g
            inkscape:groupmode="layer"
            id="layerr8"
            inkscape:label="layer4">
            <rect
                style="fill:#f4d7ee;fill-opacity:1;fill-rule:evenodd;stroke:#235213;stroke-width:0.67513;stroke-linejoin:bevel;paint-order:stroke fill markers"
                id="rect1-1"
                width="108.29942"
                height="15.261944"
                x="15.6564"
                y="54.241985" />
            <text
                xml:space="preserve"
                style="font-size:11.2889px;line-height:0px;font-family:sans-serif;-inkscape-font-specification:sans-serif;fill:#371f27;fill-opacity:0.646182;fill-rule:evenodd;stroke:#235213;stroke-width:0.470001;stroke-linejoin:bevel;stroke-dasharray:none;paint-order:stroke fill markers"
                x="45.8442"
                y="65.439781"
                id="text8"><tspan
                sodipodi:role="line"
                id="tspan8"
                style="stroke-width:0.47"
                x="45.8442"
                y="65.439781">Layer 4</tspan></text>
            </g>
            <g
            inkscape:groupmode="layer"
            id="layerr6"
            inkscape:label="layer3">
            <rect
                style="fill:#ffd5f6;fill-opacity:1;fill-rule:evenodd;stroke:#235213;stroke-width:0.67513;stroke-linejoin:bevel;paint-order:stroke fill markers"
                id="rect1-10"
                width="108.29942"
                height="15.261944"
                x="15.6564"
                y="96.607651" />
            <text
                xml:space="preserve"
                style="font-size:11.2889px;line-height:0px;font-family:sans-serif;-inkscape-font-specification:sans-serif;fill:#371f27;fill-opacity:0.646182;fill-rule:evenodd;stroke:#235213;stroke-width:0.470001;stroke-linejoin:bevel;stroke-dasharray:none;paint-order:stroke fill markers"
                x="45.995785"
                y="107.17134"
                id="text7"><tspan
                sodipodi:role="line"
                id="tspan7"
                style="stroke-width:0.47"
                x="45.995785"
                y="107.17134">Layer 3</tspan></text>
            </g>
            <g
            inkscape:groupmode="layer"
            id="layerr4"
            inkscape:label="layer2">
            <rect
                style="fill:#eeaaff;fill-opacity:1;fill-rule:evenodd;stroke:#235213;stroke-width:0.67513;stroke-linejoin:bevel;paint-order:stroke fill markers"
                id="rect1-3"
                width="108.29942"
                height="15.261944"
                x="15.6564"
                y="140.86902" />
            <text
                xml:space="preserve"
                style="font-size:11.2889px;line-height:0px;font-family:sans-serif;-inkscape-font-specification:sans-serif;fill:#371f27;fill-opacity:0.646182;fill-rule:evenodd;stroke:#235213;stroke-width:0.470001;stroke-linejoin:bevel;stroke-dasharray:none;paint-order:stroke fill markers"
                x="45.937908"
                y="151.55362"
                id="text6"><tspan
                sodipodi:role="line"
                id="tspan6"
                style="stroke-width:0.47"
                x="45.937908"
                y="151.55362">Layer 2</tspan></text>
            </g>
            <g
            inkscape:groupmode="layer"
            id="layerr3"
            inkscape:label="layer1">
            <rect
                style="fill:#dd55ff;fill-opacity:0.831373;fill-rule:evenodd;stroke:#235213;stroke-width:0.675643;stroke-linejoin:bevel;stroke-dasharray:none;paint-order:stroke fill markers"
                id="rect1-3-3"
                width="108.29934"
                height="15.261432"
                x="15.656436"
                y="185.13043" />
            <text
                xml:space="preserve"
                style="font-size:11.2889px;line-height:0px;font-family:sans-serif;-inkscape-font-specification:sans-serif;fill:#371f27;fill-opacity:0.646182;fill-rule:evenodd;stroke:#235213;stroke-width:0.470001;stroke-linejoin:bevel;stroke-dasharray:none;paint-order:stroke fill markers"
                x="41.844772"
                y="195.93611"
                id="text1-4"><tspan
                sodipodi:role="line"
                id="tspan1-8"
                style="fill:#371f27;fill-opacity:0.646182;stroke-width:0.47"
                x="41.844772"
                y="195.93611">Layer 1</tspan></text>
            </g>
            <rect
            style="fill:#b3b3b3;display:inline;"
            id="rules"
            width="54.80798"
            height="9.9117689"
            x="124.29339"
            y="-13.985335"
            inkscape:label="rules">
            <desc
                id="desc3">rule1: {
            type=layer
                model=sys/tg_test/1/short_scalAR
                layer=dishtext
                condition=not(value&gt;0 and value&lt;10)
            }
        rule2: {
            type=layer
                model=sys/tg_test/1/long_scalAR
                layer=layer2
                condition=not(value &gt; 10 and value&lt;20)
            }
        rule3: {
            type=layer
                model=sys/tg_test/1/short_scalaR
                layer=layer3
                condition=not(value&gt;20 and value&lt;30)
            }
        rule4: {
            type=layer
                model=sys/tg_test/1/short_scalaR
                layer=layer4
                condition=not(value &gt;30 and value&lt;40)
            }
        rule5: {
            type=layer
                model=test/tarantatestdevice/1/dishstate
                layer=layer5
                condition=value==Slew
            }
        rule6: {
            type=css
                model=sys/tg_test/1/long_scalar
                ifcss={&quot;stroke&quot;:&quot;red&quot;}
                elsecss={&quot;stroke&quot;:&quot;yellow&quot;}
                condition=value &gt; 50
        }
        rule7: {
            type=css
                model=sys/tg_test/1/long_scalar
                ifcss={&quot;fontSize&quot;:&quot;20px&quot;}
                elsecss={&quot;fontSize&quot;:&quot;10px&quot;}
                condition=value&gt;100
        }</desc>
            </rect>
        </g>
    </svg>


**Note**: The quotes and <,> etc. signs are converted(&quot,&lt,&gt) in above example since it is generated from Inkscape.