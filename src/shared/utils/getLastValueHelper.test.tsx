import { 
  getAttributeLastValueFromState,
  getAttributeLastQualityFromState,
  getAttributeLastTimeStampFromState,
  getAttributeLastWriteValueFromState
 } from './getLastValueHelper';

describe('getAttributeLastValueFromState', () => {
  
  it('returns the last value of an attribute', () => {
    const messages = {
      'test_device': {
        attributes: {
          'test_attribute': {
            values: [1, 2, 3],
          },
        },
      },
    };
    const device = 'test_device';
    const attribute = 'test_attribute';
    const expected = 3;

    const result = getAttributeLastValueFromState(messages, device, attribute);

    expect(result).toEqual(expected);
  });
  
  it('returns undefined if the attribute is not found', () => {
    const messages = {
      'test_device': {
        attributes: {
          'test_attribute': {
            values: [1, 2, 3],
          },
        },
      },
    };
    const device = 'test_device';
    const attribute = 'unknown_attribute';

    const result = getAttributeLastValueFromState(messages, device, attribute);

    expect(result).toEqual(undefined);
  });

  it('returns undefined if the device is not found', () => {
    const messages = {
      'test_device': {
        attributes: {
          'test_attribute': {
            values: [1, 2, 3],
          },
        },
      },
    };
    const device = 'unknown_device';
    const attribute = 'test_attribute';

    const result = getAttributeLastValueFromState(messages, device, attribute);

    expect(result).toEqual(undefined);
  });
  

  it('returns null if messages is not defined', () => {
    const device = 'test_device';
    const attribute = 'test_attribute';

    const result = getAttributeLastValueFromState(undefined, device, attribute);

    expect(result).toEqual(null);
  });
});

describe('getAttributeLastQualityFromState', () => {
  it('returns the last quality of an attribute', () => {
    const messages = {
      'test_device': {
        attributes: {
          'test_attribute': {
            quality: [1, 2, 3],
          },
        },
      },
    };
    const device = 'test_device';
    const attribute = 'test_attribute';
    const expected = 3;

    const result = getAttributeLastQualityFromState(messages, device, attribute);

    expect(result).toEqual(expected);
  });

  it('returns undefined if the attribute is not found', () => {
    const messages = {
      'test_device': {
        attributes: {
          'test_attribute': {
            quality: [1, 2, 3],
          },
        },
      },
    };
    const device = 'test_device';
    const attribute = 'unknown_attribute';

    const result = getAttributeLastQualityFromState(messages, device, attribute);

    expect(result).toEqual(undefined);
  });

  it('returns undefined if the device is not found', () => {
    const messages = {
      'test_device': {
        attributes: {
          'test_attribute': {
            quality: [1, 2, 3],
          },
        },
      },
    };
    const device = 'unknown_device';
    const attribute = 'test_attribute';

    const result = getAttributeLastQualityFromState(messages, device, attribute);

    expect(result).toEqual(undefined);
  });

  it('returns null if messages is not defined', () => {
    const device = 'test_device';
    const attribute = 'test_attribute';

    const result = getAttributeLastQualityFromState(undefined, device, attribute);

    expect(result).toBeNull();
  });
});

describe('getAttributeLastTimeStampFromState', () => {
  it('returns the last timestamp of an attribute', () => {
    const messages = {
      'test_device': {
        attributes: {
          'test_attribute': {
            timestamp: [10, 20, 30],
          },
        },
      },
    };
    const device = 'test_device';
    const attribute = 'test_attribute';
    const expected = 30;

    const result = getAttributeLastTimeStampFromState(messages, device, attribute);

    expect(result).toEqual(expected);
  });

  it('returns undefined if the attribute is not found', () => {
    const messages = {
      'test_device': {
        attributes: {
          'test_attribute': {
            timestamp: [10, 20, 30],
          },
        },
      },
    };
    const device = 'test_device';
    const attribute = 'unknown_attribute';

    const result = getAttributeLastTimeStampFromState(messages, device, attribute);

    expect(result).toEqual(undefined);
  });

  it('returns undefined if the device is not found', () => {
    const messages = {
      'test_device': {
        attributes: {
          'test_attribute': {
            timestamp: [10, 20, 30],
          },
        },
      },
    };
    const device = 'unknown_device';
    const attribute = 'test_attribute';

    const result = getAttributeLastTimeStampFromState(messages, device, attribute);

    expect(result).toEqual(undefined);
  });

  it('returns null if messages is not defined', () => {
    const device = 'test_device';
    const attribute = 'test_attribute';

    const result = getAttributeLastTimeStampFromState(undefined, device, attribute);

    expect(result).toBeNull();
  });
});

describe('getAttributeLastWriteValueFromState', () => {
  it('returns the last writevalues of an attribute', () => {
    const messages = {
      'test_device': {
        attributes: {
          'test_attribute': {
            writeValue: [10, 20, 30],
          },
        },
      },
    };
    const device = 'test_device';
    const attribute = 'test_attribute';
    const expected = 30;

    const result = getAttributeLastWriteValueFromState(messages, device, attribute);

    expect(result).toEqual(expected);
  });

  it('returns undefined if the attribute is not found', () => {
    const messages = {
      'test_device': {
        attributes: {
          'test_attribute': {
            writeValue: [10, 20, 30],
          },
        },
      },
    };
    const device = 'test_device';
    const attribute = 'unknown_attribute';

    const result = getAttributeLastWriteValueFromState(messages, device, attribute);

    expect(result).toEqual(undefined);
  });

  it('returns undefined if the device is not found', () => {
    const messages = {
      'test_device': {
        attributes: {
          'test_attribute': {
            writeValue: [10, 20, 30],
          },
        },
      },
    };
    const device = 'unknown_device';
    const attribute = 'test_attribute';

    const result = getAttributeLastWriteValueFromState(messages, device, attribute);

    expect(result).toEqual(undefined);
  });

  it('returns null if messages is not defined', () => {
    const device = 'test_device';
    const attribute = 'test_attribute';

    const result = getAttributeLastWriteValueFromState(undefined, device, attribute);

    expect(result).toBeNull();
  });
});
