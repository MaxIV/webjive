import { decimalToSexagesimal, validateEnvironment } from "./utils";

describe("decimalToSexagesimal", () => {
  it("should convert 10.9 to 10:54:0.0.000", () => {
    const result = decimalToSexagesimal(10.9, 3);
    expect(result).toBe("10:54:0.0.000");
  });

  it("should convert 42.12 to 42:7:11.1000.000", () => {
    const result = decimalToSexagesimal(42.12, 3);
    expect(result).toBe("42:7:11.1000.000");
  });

  it("should handle negative values", () => {
    const result = decimalToSexagesimal(-3.5, 2);
    expect(result).toBe("-4:30:0.0.00");
  });

  it("should handle zero and default precision", () => {
    const result = decimalToSexagesimal(0);
    expect(result).toBe("0:0:0.0.000");
  });

  it("should handle non-numeric input", () => {
    const result = decimalToSexagesimal(NaN, 3);
    expect(result).toBe("Invalid input");
  });
});

describe("validateEnvironment", () => {
  it("should accept url with valid char", () => {
    const result = validateEnvironment("http://www. google.com");
    expect(result.result).toBe("ERROR");
    expect(result.errorMessage).toContain("The environment contains not allowed char.");
  });

  it("should not accept incorrect envirnment", () => {
    const errorMessage = "Enviroment link format must be like <DOMAIN>/<NAMESPACE>/<DB>/<DEVICE or DASHBOARD>. <DB>, <DEVICE> or <DASHBOARD> are not mandatory";
    let result = validateEnvironment("https://k8s.stfc.skao.int/wrong_namespace/taranta-namespace/taranta/devices");
    expect(result.result).toBe("ERROR");
    expect(result.errorMessage).toContain(errorMessage);

    result = validateEnvironment("https://k8s.stfc.skao.int/");
    expect(result.result).toBe("ERROR");
    expect(result.errorMessage).toContain(errorMessage);

    result = validateEnvironment("https://k8s.stfc.skao.int/name_space/db/wrong_page");
    expect(result.result).toBe("ERROR");
    expect(result.errorMessage).toContain(errorMessage);
  });

  it("should accept valid", () => {
    let result = validateEnvironment("https://k8s.stfc.skao.int/taranta-namespace/taranta/devices");
    expect(result.result).toBe("OK");
    expect(result.errorMessage).toBe("");

    result = validateEnvironment("https://k8s.stfc.skao.int/taranta-namespace/taranta/devices/");
    expect(result.result).toBe("OK");
    expect(result.errorMessage).toBe("");

    result = validateEnvironment("https://k8s.stfc.skao.int/taranta-namespace/taranta/dashboard");
    expect(result.result).toBe("OK");
    expect(result.errorMessage).toBe("");

    result = validateEnvironment("https://k8s.stfc.skao.int/taranta-namespace/taranta/");
    expect(result.result).toBe("OK");
    expect(result.errorMessage).toBe("");

    result = validateEnvironment("https://k8s.stfc.skao.int/taranta-namespace/taranta");
    expect(result.result).toBe("OK");
    expect(result.errorMessage).toBe("");

    result = validateEnvironment("https://k8s.stfc.skao.int/taranta-namespace/");
    expect(result.result).toBe("OK");
    expect(result.errorMessage).toBe("");


    result = validateEnvironment("http://k8s.stfc.skao.int/taranta-namespace/");
    expect(result.result).toBe("OK");
    expect(result.errorMessage).toBe("");

    result = validateEnvironment("k8s.stfc.skao.int/taranta-namespace/");
    expect(result.result).toBe("OK");
    expect(result.errorMessage).toBe("");


    result = validateEnvironment("k8s.stfc.skao.int/*/");
    expect(result.result).toBe("OK");
    expect(result.errorMessage).toBe("");

    result = validateEnvironment("*/taranta-namespace/taranta/");
    expect(result.result).toBe("OK");
    expect(result.errorMessage).toBe("");

    result = validateEnvironment("*/*/taranta/");
    expect(result.result).toBe("OK");
    expect(result.errorMessage).toBe("");

    result = validateEnvironment("*/*");
    expect(result.result).toBe("OK");
    expect(result.errorMessage).toBe("");
  });
  
});



