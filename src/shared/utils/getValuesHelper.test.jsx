
import { 
    getAttributeValuesFromState, 
    getAttributeTimestampFromState
   } from './getValuesHelper';


   const messages = {
    "test/tarantatestdevice/1": {
        "attributes": {
            "dishstate": {
                "values": [
                    1,
                    3
                ],
                "quality": [
                    "ATTR_VALID",
                    "ATTR_INVALID"
                ],
                "timestamp": [
                    1683555840.501966,
                    0.000003
                ]
            },
            "cbfobsstate": {
                "values": [
                    3,
                    1,
                    3
                ],
                "quality": [
                    "ATTR_VALID",
                    "ATTR_VALID",
                    "ATTR_VALID"
                ],
                "timestamp": [
                    1683555840.501239,
                    1683555840.539865,
                    1683555843.549436
                ]
            }
        }
    }
};

const attributes = [
    {
        "device": "test/tarantatestdevice/1",
        "attribute": "cbfobsstate"
    }
];

describe("getAttributeValuesFromState", () => {
    
  
    it("should return an object with the correct keys and values", () => {
      const result = getAttributeValuesFromState(messages, attributes);
      expect(result["test/tarantatestdevice/1/cbfobsstate"]).toStrictEqual({ values: [ 3, 1, 3 ] });
    });
  
    it("should return an empty object if no matching device are found", () => {
      const result = getAttributeValuesFromState(messages, [
                                                            {
                                                                "device": "test/tarantatestdevice/3",
                                                                "attribute": "cbfobsstate"
                                                            }
                                                        ]);
      expect(result).toEqual([]);
    });

    it("should return an empty object if no matching attributes are found", () => {
        const result = getAttributeValuesFromState(messages, [
                                                              {
                                                                  "device": "test/tarantatestdevice/1",
                                                                  "attribute": "obsstate"
                                                              }
                                                          ]);
        expect(result).toEqual([]);
      });

});

  describe("getAttributeTimestampFromState", () => {
    
  
    it("should return an object with the correct keys and values", () => {
        const result = getAttributeTimestampFromState(messages, attributes);
        expect(result["test/tarantatestdevice/1/cbfobsstate"]).toStrictEqual({ "timestamp": [
                                                                                1683555840.501239,
                                                                                1683555840.539865,
                                                                                1683555843.549436
                                                                            ] });
      });
    
      it("should return an empty object if no matching device are found", () => {
        const result = getAttributeTimestampFromState(messages, [
                                                              {
                                                                  "device": "test/tarantatestdevice/3",
                                                                  "attribute": "cbfobsstate"
                                                              }
                                                          ]);
        expect(result).toEqual([]);
      });
  
      it("should return an empty object if no matching attributes are found", () => {
          const result = getAttributeTimestampFromState(messages, [
                                                                {
                                                                    "device": "test/tarantatestdevice/1",
                                                                    "attribute": "obsstate"
                                                                }
                                                            ]);
          expect(result).toEqual([]);
        });
  });
  