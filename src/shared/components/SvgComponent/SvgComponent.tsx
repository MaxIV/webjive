import React, { Fragment, useEffect, useState  } from "react";
import { useSelector } from "react-redux";
import { SvgLoader, SvgProxy } from "react-svgmt";
import PubSub from "pubsub-js";
import SvgTooltipComponent from "./SvgTooltip";

import {
  getAttributeLastValueFromState,
  getAttributeLastQualityFromState,
} from "../../../shared/utils/getLastValueHelper";
import { IRootState } from "../../../shared/state/reducers/rootReducer";
import {
  createDeviceWithTangoDBFullPath,
  getTangoDbFromPath,
} from "../../../dashboard/runtime/utils";
import prepareZoom, { updateZoom } from "./zoom/zoom.js";
// CSS mainly from the Synoptic application (https://gitlab.com/MaxIV/lib-maxiv-svgsynoptic)
import { splitFullPath } from "../../../dashboard/DBHelper";
import "./styles/SvgComponent.css";
import "./styles/main.css";
import "./styles/layers.css";
import "./styles/quicklinks.css";
import "./styles/tango.css";
import "./styles/tooltip.css";
import "./styles/thumbnail.css";
import { fetchAttributeMetadata, fetchAttributesList } from "../../../dashboard/utils";
import validateSVG  from "./validation/validateSVG"
import { evaluateExpression, extractSVGInfo, getConditionalLayers, getFormattedValue } from "./ExtractSVGInfo";
import TooltipIconWithModal from "../../../dashboard/components/EditCanvas/TooltipIconWithModal";
import { getTangoDBName } from "../../utils/database";

interface SvgProps {
  path: string;
  mode: string;
  layers: boolean;
  zoom: boolean;
  svgCss: string;
  viewportSvgDimensions: object;
}

const pattern = /^(model|section|alarm)=(.*)/;
let zoomObj;
var attributeMetaData, extractions, tangoDB: string = getTangoDBName(), attributeList = [];
const validationDefaultVal = {isValid: true, errors: []}

type ScannedElements = {
  id: string;
  device: string;
  attribute: string
}

function SvgComponent({
  path,
  mode,
  layers,
  zoom,
  svgCss = "",
  viewportSvgDimensions
}: SvgProps) {
  
  const [svgNode, setSVGNode] = useState(undefined);
  const [validationResult, setValidationResult] = useState(validationDefaultVal);
  const [svgElements, setSvgElements] = useState<ScannedElements[]>();

  async function activateSVG(svgNode) {
    
    setSVGNode(svgNode);
    extractions = extractSVGInfo(svgNode);
    attributeList = await fetchAttributesList(tangoDB, extractDeviceFromRules(extractions?.rules));
    const res: any = loadAndValidate(svgNode, extractions);
    setValidationResult(res);

    const scannedDevices = prepareTangoModel(svgNode);
    attributeMetaData = await fetchAttributeMetadata(scannedDevices);
    if (layers) prepareLayersManagement(svgNode);
    if (zoom) {
      zoomObj = prepareZoom(svgNode, viewportSvgDimensions);
    }
  }

  /**
   * Extract attributes from rules.
   * There are attributes used in rule to hide/show layers
   */
  function extractAttributeFromRules(rules) {
    const ruleAttributes: string[] = []
    rules?.forEach(rule => {
      try {
        const { valid, deviceFullPathWithAttr } = isValidAttribute(rule?.model);
        if(valid)
          ruleAttributes.push(deviceFullPathWithAttr);
      } catch(err) {
        console.log('Error in extracting attribute from rules: ', err)
      }
    })

    return ruleAttributes;
  }

  function extractDeviceFromRules(rules: any[]): string[] {
    const uniqueDevices = new Set<string>();

    rules.forEach(item => {
      uniqueDevices.add(item.model.split('/').slice(0, 3).join('/'));
    });

    return Array.from(uniqueDevices);
  }


  useEffect(() => {
    var token = PubSub.subscribe("Pan to", (_, bbox) => {
      if (zoomObj !== undefined && svgNode !== undefined) {
        updateZoom(svgNode, zoomObj, bbox);
      }
    });

    return () => {
      PubSub.unsubscribe(token);
    };
  }, [svgNode]);

  // Perform validation on SVG
  function loadAndValidate(svgNode, extractions) {
    if(svgNode === undefined || !attributeList?.length)
      return validationDefaultVal;

    return validateSVG(svgNode, extractions, attributeList);
  }

  useEffect(() => {
    const style = document.createElement('style')
  
    style.textContent = svgCss;
    document.head.appendChild(style);
    return () => {
      document.head.removeChild(style)
    }
  }, [svgCss]);

  /*
  Scan the description tag of each svg node for a tango model.
  For each node, create a new attribute that will help the renderer
  (better search on attribute than description content)
  Return the list of devices to subscribe
  */
  function prepareTangoModel(svgNode) {
    let scannedElements: ScannedElements[] = [];

    svgNode.querySelectorAll("desc").forEach((child) => {
      if (child.textContent) {
        const lines = child.textContent.split("\n");
        lines.forEach((line) => {
          const match = line.trim().match(pattern);
          if (match) {
            // do not process svg element that contains rules
            if (match[1] === "model"
              && "rules" !== child.parentElement?.getAttribute("inkscape:label")?.toLowerCase()
            ) {
              const kind = match[1].trim().toLowerCase();
              const isFullAttrPath = match[2].split("/").length > 3;
              let devicePathWithAttr = match[2].toLowerCase();
              if (!isFullAttrPath) {
                devicePathWithAttr = `${devicePathWithAttr}/state`;
              }

              const { valid, deviceFullPathWithAttr } = isValidAttribute(devicePathWithAttr);
              if (valid) {
                const parts = deviceFullPathWithAttr.split('/')
                const attr = parts.pop()
                const device = parts.join('/')

                scannedElements.push({
                  id: child.parentElement.id,
                  device: device,
                  attribute: attr,
                });
                child.parentElement.setAttribute(
                  "data-" + kind,
                  deviceFullPathWithAttr
                );
                child.parentElement.setAttribute(
                  "data-tooltip-id",
                  "svg-tooltip"
                );
                child.parentElement.setAttribute("data-tooltip-float", "true");
                child.parentElement.setAttribute("data-tooltip-offset", 15);
              }
            }

            child.parentElement.classList.add(match[1]);
          }
        });
      }
    });

    setSvgElements(scannedElements);
    let scannedDevices: string[] = scannedElements?.reduce<string[]>((acc, r) => {
      return [...acc, r.device+'/'+r.attribute];
    }, []);

    const extractedAttrs = extractAttributeFromRules(extractions?.rules);

    scannedDevices = scannedDevices.concat(extractedAttrs);
    scannedDevices = scannedDevices.filter((item, idx) => scannedDevices.indexOf(item) === idx);

    return scannedDevices;
  }

  function isValidAttribute(devicePathWithAttr) {
    // Check tangoDB name
    const deviceFullPathWithAttr = !getTangoDbFromPath(devicePathWithAttr) ? createDeviceWithTangoDBFullPath(
      tangoDB,
      devicePathWithAttr
    ) : devicePathWithAttr

    // Check if it exists
    return {
      valid: deviceList.nameList.some((dev) =>
        deviceFullPathWithAttr.startsWith(dev)
      ),
      deviceFullPathWithAttr
    }
  }

  function prepareLayersManagement(svg) {
    // Remove first the old Toggle layer
    const oldNode = document.getElementById("ToggleLayerNode");
    if (oldNode && oldNode.parentNode) {
      oldNode.parentNode.removeChild(oldNode);
    }
    // Create a layer for all the toggle layer buttons
    var toggleLayerNode = document.createElement("div");
    toggleLayerNode.setAttribute("id", "ToggleLayerNode");
    toggleLayerNode.classList.add("layer-toggles");
    svg.parentElement.appendChild(toggleLayerNode);
    const conditionalLayers = getConditionalLayers(extractions?.rules);
    const layersToHide = ["background", "hidden", "symbols"].concat(conditionalLayers)

    /* 
    Scan through all layers. Note in the selector, the target attribute should be all together
    with the g element to retrieve only the 2nd g level. With a space, 
    you will have all g including the children.
    */
    const layersSelector = 'svg > g > g[inkscape\\:groupmode="layer"]';

    svg.querySelectorAll(layersSelector).forEach((svgnode) => {
      const dataName = svgnode.getAttribute("inkscape:label");
      svgnode.setAttribute("data-name", dataName);
      svgnode.classList.add("layer");
      svgnode.setAttribute("display", null);
      svgnode.style.display = null;
      // Make togglable the layers
      if (dataName && !layersToHide.includes(dataName)) {
        svgnode.classList.add("togglable");
        var layerButton = document.createElement("div");
        var id = svgnode.getAttribute("id");
        var button = toggleLayerNode.appendChild(layerButton);
        button.textContent = dataName;
        button.classList.add("layer-toggle");
        button.classList.add(id);
        button.onclick = function () {
          toggleLayer(button, svgnode);
        };
      }
    });
  }

  function toggleLayer(button, layer) {
    var shown = !button.classList.contains("hidden");
    if (shown) {
      button.classList.add("hidden");
      layer.classList.add("hidden");
    } else {
      button.classList.remove("hidden");
      layer.classList.remove("hidden");
    }
  }

  const extractSynopticValues = (messages: Object) => {
    let svgValues: Array<{
      deviceName: string;
      attributeName: string;
      selector: string;
      lastValue: string;
      attributeQuality: string;
      attributeDataType: string;
    }> = [];

    if (svgElements) {
      svgElements.forEach((element) => {
        const deviceName = element.device;
        const attributeName = element.attribute
        let lastValue =  getAttributeLastValueFromState(messages, deviceName, attributeName);

        if (undefined === lastValue) return;

        lastValue = String(lastValue);
        const fullName = deviceName + "/" + attributeName;
        //Pull enum label for enum types
        lastValue =
          attributeMetaData?.[fullName]?.["enumlabels"]?.[lastValue] ||
          lastValue;

        const attributeQuality = getAttributeLastQualityFromState(
          messages,
          deviceName,
          attributeName
        );

        const attributeDataType =
          lastValue === "true" || lastValue === "false" ? "boolean" : "state";
        const [, devName] = splitFullPath(deviceName);
        svgValues.push({
          deviceName: devName,
          attributeName: attributeName,
          selector: element.id,
          lastValue: lastValue,
          attributeQuality: attributeQuality,
          attributeDataType: attributeDataType,
        });
      });
    }

    return svgValues;
  };

  const synopticValues = useSelector((state: IRootState) => {
    return extractSynopticValues(state.messages);
  });

  const deviceList = useSelector((state: IRootState) => {
    return state.deviceList;
  });

  // TODO: what does the onElement Selected shoud do?
  const onElementSelected = (node) => {
    // console.log("Node selected", node);
  };

  useEffect(() => {
    const useThrottledLayerRender = () => {
      const layerRules = extractions?.rules?.filter(r => r.type === "layer")
      if (!layerRules) return;
      layerRules?.forEach(rule => {
        try {
          if (rule?.type === "layer") {
            // Find the model/attr which is used in this rule's condition
            const objAttr = synopticValues.find(r => (r?.deviceName+'/'+r?.attributeName) === rule?.model);
            if (objAttr) {
              // If rule is found associated with current item then process
              const show = evaluateExpression(rule?.condition, {value: objAttr?.lastValue});
              
              const svgElements = document.querySelectorAll('svg *');
              // Filter elements with the specific custom attribute
              const filteredElements = Array.from(svgElements).filter(element =>
                  element.getAttribute('inkscape:label') === rule.layer
              );

              filteredElements.forEach(element => {
                if (show) {
                  element.classList.remove('hide')
                } else {
                  element.classList.add('hide')
                }
              });
            }
          }
        } catch(err) {
          console.log('Rule Error: ', err, ' for rule: ', rule)
        }
      })
    }

    if (mode === "run") {
      const interval = setInterval(useThrottledLayerRender, 125);

      return () => clearInterval(interval);
    } else {
      return () => {}
    }
  }, [mode, synopticValues])

  function renderSynoptic() {
    if (deviceList.nameList && deviceList.nameList.length > 0 && path) {
      return (
        <Fragment>
          <SvgLoader
            height="100%"
            width="100%"
            onSVGReady={activateSVG}
            svgXML={path}
            text-align="center"
          >
            {synopticValues.map((item, ix) => {
              return (
                <SvgProxy
                  key={ix}
                  onElementSelected={onElementSelected}
                  class={item.attributeDataType}
                  selector={"#"+item.selector}
                  data-value={item.lastValue}
                  data-quality={item.attributeQuality}
                >
                  {item.attributeName !== "state" ? getFormattedValue(extractions, item) : ""}
                </SvgProxy>
              );
            })}
            {<SvgTooltipComponent />}
          </SvgLoader>
        </Fragment>
      );
    } else if (mode === "run") {
      return <p> Waiting for the list of existing devices </p>;
    } else {
      return <p> Waiting for a synoptic to be selected </p>;
    }
  }
  return <div className="SvgComponent">
    {renderSynoptic()}
    {!validationResult?.isValid &&
      <TooltipIconWithModal messages={validationResult?.errors} />
    }
  </div>;
}

export default SvgComponent;