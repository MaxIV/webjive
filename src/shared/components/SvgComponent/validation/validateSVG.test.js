import { validateLinks, validateSVG } from './validateSVG';
import { validateLayers, validateAttributes } from './validateSVG';
import { extractSVGInfo } from '../ExtractSVGInfo';

jest.mock('../ExtractSVGInfo');

describe('validateSVG', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('returns valid with no errors when all rules are correct', () => {
    const svgString = `<svg xmlns="http://www.w3.org/2000/svg">
      <g inkscape:label="ExistingLayer"></g>
    </svg>`;

    const parser = new DOMParser();
    const svgContent = parser.parseFromString(svgString, 'image/svg+xml').documentElement;
    const elements = {
      rules: [
        { type: 'css', layer: 'NonExistentLayer', condition: 'value < 0' },
      ],
    }

    const result = validateSVG(svgContent, elements, []);
    expect(result.isValid).toBe(true);
    expect(result.errors).toHaveLength(0);
  });

  it('validate layers', () => {
    const svgString = `<svg xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape">
      <g inkscape:label="SomeLayer"></g>
    </svg>`;

    const parser = new DOMParser();
    const svgContent = parser.parseFromString(svgString, 'image/svg+xml').documentElement;
    const elements = {
      rules: [
        {
          type: 'layer',
          layer: 'SomeLayer',
          condition: 'value > 0'
        }
      ],
    }

    const result = validateSVG(svgContent, elements);
    expect(result.isValid).toBe(true);
    expect(result.errors).toHaveLength(0);
  });

  it('no rules', () => {
    const svgString = `<svg xmlns="http://www.w3.org/2000/svg">
      <g inkscape:label="ExistingLayer"></g>
    </svg>`;

    const parser = new DOMParser();
    const svgContent = parser.parseFromString(svgString, 'image/svg+xml').documentElement;
    const elements = {
      rules: [
      ],
    }

    const result = validateSVG(svgContent, elements);
    expect(result.isValid).toBe(true);
    expect(result.errors).toHaveLength(0);
  });

  it('no rules', () => {
    const svgString = `<svg xmlns="http://www.w3.org/2000/svg">
      <g inkscape:label="ExistingLayer"></g>
    </svg>`;

    const parser = new DOMParser();
    const svgContent = parser.parseFromString(svgString, 'image/svg+xml').documentElement;
    const elements = {

    }

    const result = validateSVG(svgContent, elements);
    expect(result.isValid).toBe(true);
    expect(result.errors).toHaveLength(0);
  });

  it('returns valid with  errors when one rule is not correct', () => {
    const svgString = `<svg xmlns="http://www.w3.org/2000/svg">
      <g inkscape:label="ExistingLayer"></g>
    </svg>`;

    const parser = new DOMParser();
    const svgContent = parser.parseFromString(svgString, 'image/svg+xml').documentElement;
    const elements = {
      rules: [
        { type: 'invalidRule1'},
        { type: 'invalidRule2'},
      ],
    }

    const result = validateSVG(svgContent, elements);
    expect(result.isValid).toBe(false);
    expect(result.errors).toHaveLength(2);
  });
});

describe('validateLayers', () => {
  it('returns an error if the specified layer does not exist in the SVG', () => {
    const elements = {
      rules: [
        {
          type: 'layer',
          layer: 'NonExistentLayer',
          condition: 'value > 0'
        }
      ]
    };
    const svgString = `<svg xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape">
      <g inkscape:label="ExistingLayer"></g>
    </svg>`;

    const errors = validateLayers(elements, svgString);
    expect(errors).toHaveLength(1);
    expect(errors[0].error).toMatch(/Layer "NonExistentLayer" not found/);
  });

  it('returns an error if condition is missing', () => {
    const elements = {
      rules: [
        {
          type: 'layer',
          layer: 'SomeLayer'
          // no condition
        }
      ]
    };
    const svgString = `<svg xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape">
      <g inkscape:label="SomeLayer"></g>
    </svg>`;

    const errors = validateLayers(elements, svgString);
    expect(errors).toHaveLength(1);
    expect(errors[0].error).toMatch(/Condition is empty or missing/);
  });

  it('returns an error if condition contains invalid characters', () => {
    const elements = {
      rules: [
        {
          type: 'layer',
          layer: 'SomeLayer',
          condition: 'value > 0#' // '#' is not allowed
        }
      ]
    };
    const svgString = `<svg xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape">
      <g inkscape:label="SomeLayer"></g>
    </svg>`;
  
    const errors = validateLayers(elements, svgString);
    expect(errors).toHaveLength(1);
    expect(errors[0].error).toMatch(/contains invalid characters/);
  });

  it('returns an error if condition does not contain a valid operator', () => {
    const elements = {
      rules: [
        {
          type: 'layer',
          layer: 'SomeLayer',
          condition: 'value' // no operator like >, <, ==, etc.
        }
      ]
    };
    const svgString = `<svg xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape">
      <g inkscape:label="SomeLayer"></g>
    </svg>`;

    const errors = validateLayers(elements, svgString);
    expect(errors).toHaveLength(1);
    expect(errors[0].error).toMatch(/does not contain a valid operator/);
  });

  it('returns an error if condition does not reference "value"', () => {
    const elements = {
      rules: [
        {
          type: 'layer',
          layer: 'SomeLayer',
          condition: '10 > 0'
        }
      ]
    };
    const svgString = `<svg xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape">
      <g inkscape:label="SomeLayer"></g>
    </svg>`;

    const errors = validateLayers(elements, svgString);
    expect(errors).toHaveLength(1);
    expect(errors[0].error).toMatch(/does not reference "value"/);
  });

  it('returns an error if condition is always true', () => {
    // "value == value" will always be true for any test value
    const elements = {
      rules: [
        {
          type: 'layer',
          layer: 'SomeLayer',
          condition: 'value == value'
        }
      ]
    };
    const svgString = `<svg xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape">
      <g inkscape:label="SomeLayer"></g>
    </svg>`;

    const errors = validateLayers(elements, svgString);
    expect(errors).toHaveLength(1);
    expect(errors[0].error).toMatch(/is always true/);
  });

  it('returns an error if condition is always false', () => {
    // Given the test values [-100, 0, 50, 100, 1000], "value > 1001" is always false
    const elements = {
      rules: [
        {
          type: 'layer',
          layer: 'SomeLayer',
          condition: 'value > 1001'
        }
      ]
    };
    const svgString = `<svg xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape">
      <g inkscape:label="SomeLayer"></g>
    </svg>`;

    const errors = validateLayers(elements, svgString);
    expect(errors).toHaveLength(1);
    expect(errors[0].error).toMatch(/is always false/);
  });

  it('returns no errors if layer exists and condition can be both true and false', () => {
    // "value > 0" will be true for positive test values and false for -100
    const elements = {
      rules: [
        {
          type: 'layer',
          layer: 'SomeLayer',
          condition: 'value > 0'
        }
      ]
    };
    const svgString = `<svg xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape">
      <g inkscape:label="SomeLayer"></g>
    </svg>`;

    const errors = validateLayers(elements, svgString);
    expect(errors).toHaveLength(0);
  });

  it('returns an error if the condition is invalid or cannot be evaluated', () => {
    const elements = {
      rules: [
        {
          type: 'layer',
          layer: 'SomeLayer',
          // This condition is incomplete and will cause a syntax error
          condition: 'value >'
        }
      ]
    };
    const svgString = `<svg xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape">
      <g inkscape:label="SomeLayer"></g>
    </svg>`;
  
    const errors = validateLayers(elements, svgString);
    expect(errors).toHaveLength(1);
    expect(errors[0].error).toMatch(/invalid or could not be evaluated/);
  });

  it('returns an error if the condition does not evaluate to a boolean', () => {
    const elements = {
      rules: [
        {
          type: 'layer',
          layer: 'SomeLayer',
          // This condition returns a number, not a boolean
          condition: 'value || 10'
        }
      ]
    };
    const svgString = `<svg xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape">
      <g inkscape:label="SomeLayer"></g>
    </svg>`;
  
    const errors = validateLayers(elements, svgString);
    expect(errors).toHaveLength(1);
    expect(errors[0].error).toMatch(/did not evaluate to a boolean/);
  });

  it('returns an error if the anchor link does not evaluate to a boolean', () => {
    const svgString = `<svg xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape">
      <g inkscape:label="SomeLayer">
        <a id="a2" target="_blank" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:actuate="onLoad" xlink:href="s://www.youtube.com/watch?v=W9y_D90L8Jo">
          <text xml:space="preserve" x="159.30379" y="42.244789" id="text1-1"><tspan xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" sodipodi:role="line" id="tspan1-6" style="stroke-width:0.47" x="159.30379" y="42.244789">Short Scalar</tspan></text>
        </a>
        <a id="a3" target="_blank" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:title="new text" xlink:show="new" xlink:actuate="onLoad" xlink:href="https://www.youtube.com/watch?v=W9y_D90L8Jo">
          <text xml:space="preserve" x="159.30379" y="42.244789" id="text1-1"><tspan xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" sodipodi:role="line" id="tspan1-6" style="stroke-width:0.47" x="159.30379" y="42.244789">Short Scalar</tspan></text>
        </a>
      </g>
    </svg>`;

    const parser = new DOMParser();
    const svgContent = parser.parseFromString(svgString, 'image/svg+xml').documentElement;

    const errors = validateLinks(svgContent);
    expect(errors).toHaveLength(1);
    expect(errors[0].error).toEqual("Anchor with Id \"a2\" has invalid link \"s://www.youtube.com/watch?v=W9y_D90L8Jo\".");
  });

  it('returns no errors when all models are valid', () => {
    const elements = {
        rules: [
            { model: 'device1/attribute1' },
            { model: 'device2/attribute2' },
        ],
    };
    const attributeList = [
        { deviceName: 'device1', name: 'attribute1' },
        { deviceName: 'device2', name: 'attribute2' },
    ];

    const errors = validateAttributes(elements, attributeList);
    expect(errors).toHaveLength(0);
});

it('returns an error when a model is missing from the attributeList', () => {
    const elements = {
        rules: [
            { model: 'device1/attribute1' },
            { model: 'device2/attribute3' }, // This model is not in the list
        ],
    };
    const attributeList = [
        { deviceName: 'device1', name: 'attribute1' },
        { deviceName: 'device2', name: 'attribute2' },
    ];

    const errors = validateAttributes(elements, attributeList);
    expect(errors).toHaveLength(1);
    expect(errors[0]).toEqual({
        item: { model: 'device2/attribute3' },
        error: "Attribute 'device2/attribute3' not present.",
    });
});

it('returns no errors when elements.rules is undefined', () => {
    const elements = {}; // No rules defined
    const attributeList = [
        { deviceName: 'device1', name: 'attribute1' },
    ];

    const errors = validateAttributes(elements, attributeList);
    expect(errors).toHaveLength(0);
});


it('handles empty rules gracefully', () => {
    const elements = { rules: [] }; // Empty rules
    const attributeList = [
        { deviceName: 'device1', name: 'attribute1' },
    ];

    const errors = validateAttributes(elements, attributeList);
    expect(errors).toHaveLength(0);
});

it('handles cases with partially valid models', () => {
    const elements = {
        rules: [
            { model: 'device1/attribute1' },
            { model: 'device2/attribute3' },
        ],
    };
    const attributeList = [
        { deviceName: 'device1', name: 'attribute1' },
    ];

    const errors = validateAttributes(elements, attributeList);
    expect(errors).toHaveLength(1);
    expect(errors[0]).toEqual({
        item: { model: 'device2/attribute3' },
        error: "Attribute 'device2/attribute3' not present.",
    });
});

});
