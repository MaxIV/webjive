import React, { useCallback, useEffect, useState } from "react";
import { SvgLoader } from "react-svgmt";
import PubSub from "pubsub-js";
import { getViewBox } from "../zoom/zoom";
import "../styles/thumbnail.css";

const updateIndicator = (
  svgNode,
  transform,
  innerWidth,
  innerHeight,
  viewportSvgDimensions
) => {
  // Calculate the ratio between the thumbnail and the svg elements sizes
  const widthRatio = innerWidth / svgNode.viewBox.animVal.width;
  const heightRatio = innerHeight / svgNode.viewBox.animVal.height;
  const bbox = getViewBox(viewportSvgDimensions, transform);
  // Multiply the ratio with the viewport
  const left = `${bbox.x * widthRatio}px`;
  const top = `${bbox.y * heightRatio}px`;
  const width = `${bbox.width * widthRatio}px`;
  const height = `${bbox.height * heightRatio}px`;
  return { left, top, width, height };
};

function Thumbnail({ svgFile, viewportSvgDimensions }) {
  const [isInvisible, setIsInvisible] = useState({ isHidden: false });
  const [button, setButton] = useState("-");
  const [indicatorStyle, setIndicatorStyle] = useState({
    width: "100",
    height: "100",
  });
  const [transform, setTransform] = useState(null);
  const [svgNode, setSVGNode] = useState();
  const [innerWidth, setInnerWidth] = useState(0);
  const [innerHeight, setInnerHeight] = useState(0);
  const innerRef = useCallback(
    (node) => {
      if (node !== null && transform) {
        // node.getBoundingClientRect().width/height, which is a more elegant solution
        // gives the same value, but it breaks the indicator somehow
        setInnerWidth(node.querySelector("svg").clientWidth);
        setInnerHeight(node.querySelector("svg").clientHeight);
      }
    },
    [transform]
  );

  useEffect(() => {
    var token = PubSub.subscribe("current bbox", (_, transformData) => {
      setTransform({ ...transformData });
    });

    return () => {
      PubSub.unsubscribe(token);
    };
  }, []);

  useEffect(() => {
    const url = window.location.href;
    if (url.includes("id")) {
      setIsInvisible({ isHidden: false });
      setButton("-");
    } else {
      setIsInvisible({ isHidden: true });
      setButton("+");
    }
  }, [svgFile]);

  const toggleVisibility = () => {
    if (isInvisible.isHidden) {
      setIsInvisible({ isHidden: false });
      setButton("-");
    } else {
      setIsInvisible({ isHidden: true });
      setButton("+");
    }
  };

  let style = { visibility: isInvisible.isHidden ? "hidden" : "visible" };

  // eslint-disable-next-line
  useEffect(() => {
    if (svgNode !== undefined && transform) {
      const indicatorStyle = updateIndicator(
        svgNode,
        transform,
        innerWidth,
        innerHeight,
        viewportSvgDimensions
      );
      setIndicatorStyle(indicatorStyle);
    }
  }, [svgNode, transform, innerWidth, innerHeight, viewportSvgDimensions]);

  // re-center the view on click
  const panTo = (svgNode, event) => {
    const scale = svgNode.width.animVal.value / viewportSvgDimensions.width;
    if (event !== undefined)
      var bbox = getViewBox(viewportSvgDimensions, transform),
        padded = {
          x: event.nativeEvent.layerX * scale - bbox.width / 2,
          y: event.nativeEvent.layerY * scale - bbox.height / 2,
          width: bbox.width,
          height: bbox.height,
          clickX: event.nativeEvent.layerX,
          clickY: event.nativeEvent.layerY,
          transform: transform,
          thumbnailSvgNode: svgNode,
        };
    const indicatorStyle = updateIndicator(
      svgNode,
      transform,
      innerWidth,
      innerHeight,
      viewportSvgDimensions
    );
    setIndicatorStyle(indicatorStyle);
    PubSub.publish("Pan to", padded);
  };

  return (
    <div>
      <div className="thumbnail" width={"20%"} height={"20%"} style={style}>
        <div className="inner" ref={innerRef}>
          <div className="indicator" style={indicatorStyle}></div>
          <div title="synoptic">
            <SvgLoader
              height="100%"
              width="100%"
              onSVGReady={setSVGNode}
              svgXML={svgFile}
              text-align="center"
              onClick={(event) => panTo(svgNode, event)}
            />
          </div>
        </div>
      </div>
      <div
        className="thumbnail-toggle"
        title="Hide/show thumbnail"
        onClick={toggleVisibility}
        style={{ cursor: "pointer" }}
      >
        {button}
      </div>
    </div>
  );
}

export default Thumbnail;
