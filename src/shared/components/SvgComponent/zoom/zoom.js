import panzoom from "panzoom";
import PubSub from "pubsub-js";
import util from "../util.js";
// calculate the largest scale at which the whole image fits in
// the window.
// This seems to conflict with the panzoom library

// get the current view box
export const getViewBox = (viewportSvgDimensions, transform) => {
  return {
    width: viewportSvgDimensions.width / transform.scale,
    height: viewportSvgDimensions.height / transform.scale,
    x: -parseFloat(transform.x / transform.scale),
    y: -parseFloat(transform.y / transform.scale),
  };
};

const fitImageInWindow = (
  windowWidth,
  windowHeight,
  imageWidth,
  imageHeight
) => {
  var windowRatio = windowWidth / windowHeight,
    imageRatio = imageWidth / imageHeight;

  if (windowRatio < imageRatio) {
    return windowWidth / imageWidth;
  }
  return windowHeight / imageHeight;
};

// setup the mouse pan/zoom behavior
function addZoomBehaviour(svg, svgMain) {
  svg.querySelectorAll("svg > g > g > g").forEach((node) => {
    var name = node.getAttribute("inkscape:label") || "";
    var match = /zoom(\d)/.exec(name);
    if (match) {
      // This is a zoom layer
      var level = parseInt(match[1]);
      node.classList.add("zoom");
      node.classList.add("level" + level);
      node.style.display = null;
    }
  });
  return panzoom(svgMain);
}

// define the area to be visible
// replace with zoom.zoomTo(x,y,scale)
const updateZoom = async (svg, zoom, bbox, config) => {
  var parent = svg.parentElement;
  var elWidth = parent.clientWidth,
    elHeight = parent.clientHeight;
  var windowBox = svg.getBoundingClientRect();
  var svgMain = svg.querySelector("svg > g");
  var imageBox = svgMain.getBBox();
  const minimumScale = fitImageInWindow(
    windowBox.width,
    windowBox.height,
    imageBox.width,
    imageBox.height
  );
  config = config || {};
  var zoomSteps = config.zoomSteps || [1, 5, 10],
    maxZoom = zoomSteps.slice(-1)[0];
  var scale = Math.min(elWidth / bbox.width, elHeight / bbox.height);
  let mainSynopticToThumbnailRatio =
    parseInt(bbox.thumbnailSvgNode.getAttribute("viewBox").split(" ")[2]) /
    bbox.thumbnailSvgNode.getBoundingClientRect().width;
  scale = Math.min(scale, minimumScale * maxZoom);
  let effectiveZoomScale = scale >= 1.5 ? scale : 1.5;

  var x =
      -bbox.clickX * mainSynopticToThumbnailRatio * bbox.transform.scale +
      elWidth / 2,
    y =
      -bbox.clickY * mainSynopticToThumbnailRatio * bbox.transform.scale +
      elHeight / 2;

  // Simply using 'zoomTo' is broken a bit. The approach below is more intuitive.
  // First, we move To desired point, making x,y the center of viewport.
  // Then we zoom with the center being x,y point. We need to adjust zoom coordinates
  // Because it treats the x,y (as in 'zoomTo(x,y,scale)') arguments as the corner's
  // coordinates, not the center point's coords.
  await zoom.moveTo(x, y);
  var zoomSelector = svg.querySelector(".zoom.level2");
  if (zoomSelector) {
    if (zoomSelector.classList.contains("hidden")) {
      await zoom.zoomTo(elWidth / 2, elHeight / 2, effectiveZoomScale);
    }
  }
};

const prepareZoom = (svg, viewportSvgDimensions) => {
  /*
  config.zoomSteps is a list of numbers. The first number
  determines at which scale (relative to full view) we switch
  from detail level 0 to 1, and so on. 
  Setting the first number to 1 means that the lowest detail level is only used
  when fully zoomed out, showing the whole picture.  
  The end points of zoomSteps also limits user zooming.  If the
  number of steps is smaller than the number of zoom levels
  in the SVG, those higher zoom levels will not be visible.
  */

  //TODO: Secure to get the main layer e.g main is maybe not the only top layer
  var svgMain = svg.querySelector("svg > g"); // the toplevel group
  var zoomNodes = svg.querySelectorAll("svg > g > g > g");
  var zoomLevels = [];
  zoomNodes.forEach((node) => {
    var name = node.getAttribute("inkscape:label"),
      match = /zoom(\d)/.exec(name);
    if (match) {
      zoomLevels.push(parseInt(match[1]));
    }
  });

  if (!svgMain) return null;

  var minimumScale = 1,
    oldZoomLevel;

  var windowBox = svg.getBoundingClientRect();
  var imageBox = svgMain.getBBox();
  minimumScale = fitImageInWindow(
    windowBox.width,
    windowBox.height,
    imageBox.width,
    imageBox.height
  );

  var zoom = addZoomBehaviour(svg, svgMain);
  zoom.on("transform", (event) => {
    var transform = event.getTransform();
    PubSub.publish("current bbox", transform);
    const visibleNodes = updateZoomLevel(transform);
    PubSub.publish("current visibleNodes", visibleNodes);
  });

  // update the zoom levels so that the one that corresponds to
  // the current scale is visible
  // pan will trigger this function to run
  const updateZoomLevel = (transform) => {
    var relativeScale = transform.scale / minimumScale,
      zoomLevel;
    if (relativeScale <= Math.max(...zoomLevels)) {
      zoomLevel = Math.floor(relativeScale);
    } else {
      zoomLevel = Math.max(...zoomLevels);
    }

    if (zoomLevel !== oldZoomLevel) {
      var levelClass = ".level" + zoomLevel;
      svgMain
        .querySelectorAll("g.zoom:not(" + levelClass + ")")
        .forEach((node) => {
          node.classList.add("hidden");
          node.classList.add("really-hidden");
        });
      svgMain.querySelectorAll("g.zoom" + levelClass).forEach((node) => {
        node.classList.remove("hidden");
        node.classList.remove("really-hidden");
      });
      oldZoomLevel = zoomLevel;
    }
    return updateVisibility(svgMain, transform);
  };

  const selectShownThings = (svgMain) => {
    return svgMain.querySelectorAll(
      "g.layer:not(.hidden) >  .model, .state, .boolean" +
        "g.layer:not(.hidden) > g:not(.zoom) .model, .state, .boolean" +
        "g.layer:not(.hidden) > g.zoom:not(.hidden), .model, .state, .boolean" +
        "g.layer:not(.hidden) > g.zoom:not(.hidden) > .model, .state, .boolean"
    );
  };

  function getBBox(node) {
    try {
      var bboxes = [];
      var bbox;
      bbox = util.transformedBoundingBox(node);
      // we'll also store the bbox in the node's data for easy
      // access.
      bboxes.push(bbox);
      return bboxes;
    } catch (e) {
      console.log(e);
      // This probably means that the element is not displayed.
      return [];
    }
  }
  // return whether a given element is currently in view
  function isInView(bboxes, vbox) {
    if (!bboxes || bboxes.length === 0) return false;
    return bboxes.some(function(bbox) {
      // is any part of the bbox inside the vbox?
      return (
        bbox.x > vbox.x - bbox.width &&
        bbox.y > vbox.y - bbox.height &&
        bbox.x < vbox.x + vbox.width &&
        bbox.y < vbox.y + vbox.height
      );
    });
  }

  const updateVisibility = (svgMain, vbox) => {
    vbox = getViewBox(viewportSvgDimensions, vbox);
    var sel = selectShownThings(svgMain),
      visibleNodes = [];
    sel.forEach((node) => {
      var bbox = getBBox(node),
        visible = isInView(bbox, vbox);
      util.setClass(node, "hidden", visible);
      if (visible) {
        visibleNodes.push(node.dataset.model);
      }
    });
    return visibleNodes;
  };
  return zoom;
};

export default prepareZoom;
export { fitImageInWindow, updateZoom };
