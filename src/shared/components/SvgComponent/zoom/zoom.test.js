import { fitImageInWindow, getViewBox } from "./zoom";

test("fitImageInWindow provides correct ratio for landscape image", () => {
  const windowWidth = 1920;
  const windowHeight = 1080;
  const imageWidth = 1920;
  const imageHeight = 1080;

  const scale = fitImageInWindow(
    windowWidth,
    windowHeight,
    imageWidth,
    imageHeight
  );

  expect(scale).toBe(1);
});

test("fitImageInWindow provides correct ratio for portrait image", () => {
  const windowWidth = 1920;
  const windowHeight = 1080;
  const imageWidth = 1080;
  const imageHeight = 1920;

  const scale = fitImageInWindow(
    windowWidth,
    windowHeight,
    imageWidth,
    imageHeight
  );

  expect(scale).toBeCloseTo(0.5625, 4);
});

test("fitImageInWindow provides correct ratio for portrait image, with different orientation", () => {
  const windowWidth = 1080;
  const windowHeight = 1920;
  const imageWidth = 1920;
  const imageHeight = 1080;

  const scale = fitImageInWindow(
    windowWidth,
    windowHeight,
    imageWidth,
    imageHeight
  );

  expect(scale).toBeCloseTo(0.5625, 4);
});

test("getViewBox image proper box view", () => {
  const svg = document.createElement("svg");
  const transform = { scale: 2, x: 0, y: 0 };
  const dimensions = { width: svg.clientWidth, height: svg.clientHeight };

  const viewBox = getViewBox(dimensions, transform);

  expect(viewBox).toEqual({ width: 0, height: 0, x: -0, y: -0 });
});
