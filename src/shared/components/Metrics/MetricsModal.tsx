import React, { useState } from "react";
import Modal from "../../modal/components/Modal/Modal";
import { Button, Tab, Tabs, Alert } from "react-bootstrap";
import { IRootState } from "../../../shared/state/reducers/rootReducer";
import { useSelector } from "react-redux";
import { SubscribedAttr, Query, Mutation } from "../../state/types/types";
import { IAttributesState } from "../../../shared/state/reducers/attributes";
import { getTangoDB } from "../../../dashboard/dashboardRepo";
import { getSubs } from "../../utils/getSubs";
import { getDeviceFromPath } from "../../../dashboard/runtime/utils";
import {
  WIDGET_MISSING_DEVICE,
  WIDGET_WARNING,
} from "../../state/actions/actionTypes";
import { mapVariableNameToDevice } from "../../utils/DashboardVariables";

const tagStyle = {
  display: "inline-block",
  backgroundColor: "#e0e0e0",
  color: "#333",
  padding: "5px 10px",
  borderRadius: "15px",
  fontSize: "14px",
  fontWeight: 500,
  margin: "2px",
};

interface Props {
  setState: (obj) => void;
}

const checkAttributeRegistered = (widget, deviceList, warningDevice) => {
  const { valid } = widget;

  if ("BOX" === widget.type && widget.innerWidgets.length > 0) {
    widget.innerWidgets.forEach((innerWidget) => {
      checkAttributeRegistered(innerWidget, deviceList, warningDevice);
    });
  } else {
    if ([WIDGET_WARNING, WIDGET_MISSING_DEVICE].includes(valid)) {
      const device = widget.inputs?.attribute?.device;
      if (device) {
        warningDevice.push(device);
      }
    }
  }
  return warningDevice;
};

const MetricsModal: React.FC<Props> = ({ setState }) => {
  const devices: IAttributesState = useSelector((state: IRootState) => {
    return state.attributes;
  });

  const [show, setShow] = useState(true);

  const subscribedAttrsTangoGQL: SubscribedAttr[] = useSelector(
    (state: IRootState) => {
      return state.communicationHealth?.metrics?.subscribedAttrs || [];
    }
  );

  const pubSub = useSelector((state: IRootState) => {
    return state.communicationHealth?.metrics?.pubSub || false;
  });

  const query: Query[] = useSelector((state: IRootState) => {
    return state.communicationHealth?.metrics?.executionTime?.query || [];
  });

  const mutation: Mutation[] = useSelector((state: IRootState) => {
    return state.communicationHealth?.metrics?.executionTime?.mutation || [];
  });

  const dashboardVariables = useSelector((state: IRootState) => {
    return state.selectedDashboard?.variables || [];
  });

  const metricsError: string = useSelector((state: IRootState) => {
    return state.communicationHealth?.error;
  });

  let widgets = useSelector((state: IRootState) => {
    return state.selectedDashboard.widgets;
  });

  if (dashboardVariables?.length > 0)
    widgets = mapVariableNameToDevice(
      Object.values(widgets),
      dashboardVariables
    );

  //TODO: retrieve device from all attributes subscribed also from others tangoDB
  const tangoDB = getTangoDB();
  const subscribedAttrsTarantaDevice: string[] = [];
  for (const key in devices) {
    if (Object.hasOwnProperty.call(devices, key)) {
      const innerObj = devices[key];
      for (const innerKey in innerObj) {
        if (Object.hasOwnProperty.call(innerObj, innerKey)) {
          subscribedAttrsTarantaDevice.push(
            `${key.split(tangoDB + "://")[1]}/${innerKey}`
          );
        }
      }
    }
  }
  let subscribedAttrsTaranta: string[] = [];

  if (Object.keys(widgets).length > 0) {
    const subs = getSubs(widgets) || [];
    subscribedAttrsTaranta = subs.reduce((acc: string[], r) => {
      acc?.push(getDeviceFromPath(r));
      return acc;
    }, []);
  }
  subscribedAttrsTaranta = Array.from(
    new Set([...subscribedAttrsTaranta, ...subscribedAttrsTarantaDevice])
  );

  const devicesWithListenersRunning: SubscribedAttr[] = subscribedAttrsTangoGQL.filter(
    (device) =>
      subscribedAttrsTaranta
        .map((attr) => attr.toLowerCase())
        .includes(device.name.toLowerCase()) &&
      device.listeners > 0 &&
      device.deviceAccessible === true
  );

  const devicesWithListenersNotRunning: SubscribedAttr[] = subscribedAttrsTangoGQL.filter(
    (device) =>
      subscribedAttrsTaranta.includes(device.name) &&
      device.listeners > 0 &&
      device.deviceAccessible === false
  );

  let invalidAttribute: string[] = [];
  Object.entries(widgets).forEach((elements) => {
    checkAttributeRegistered(
      elements["1"],
      subscribedAttrsTaranta,
      invalidAttribute
    );
  });

  return (
    <Modal title={`TangoGQL Health Status & Communication Metrics`} size="lg">
      <Modal.Body>
        {metricsError && (
          <div>
            <div className="h5">
              Error while fetching metrics data from TangoGQL:
            </div>
            <code>{metricsError}</code>
          </div>
        )}
        {"true" === sessionStorage.getItem("TANGOGQL_ARIADNE") && show && (
          <Alert variant="warning" onClose={() => setShow(false)} dismissible>
            Metrics data is not available for the current version of TangoGQL.
          </Alert>
        )}
        {!metricsError && (
          <Tabs
            defaultActiveKey={"subscription"}
            id="metrics-modal"
            className="mb-3"
          >
            <Tab eventKey="subscription" title="Subscriptions">
              <div className="metrics-wrapper">
                <h4>Data received</h4>
                <h6>
                  Subscribed attributes that do provide data from TangoGQL
                </h6>
                <div className="metrics-wrapper">
                  <div
                    id="metrics-view"
                    className={`table-responsive table-responsive table-sm `}
                  >
                    {devicesWithListenersRunning.length === 0 && (
                      <div className="font-italic ml-3">
                        No data coming from TangoGQL
                      </div>
                    )}
                    {devicesWithListenersRunning.length > 0 && (
                      <table
                        className={`table table-bordered  table-hover align-items-center justify-content-center"`}
                      >
                        <tbody>
                          <tr className="thead-light">
                            <th>Attribute</th>
                            <th>Subscription type</th>
                            <th>Listeners</th>
                            <th>Pub/Sub</th>
                            <th>Device Accessible</th>
                          </tr>
                          {devicesWithListenersRunning.map((element, i) => {
                            return (
                              <tr key={i}>
                                <td>{element.name}</td>
                                <td>{element.eventType}</td>
                                <td>{element.listeners}</td>
                                <td>{String(pubSub)}</td>
                                <td>
                                  <span
                                    style={{
                                      backgroundColor: "green",
                                      padding: "3px",
                                      borderRadius: "2px",
                                      color: "white",
                                    }}
                                  >
                                    {element.deviceAccessible ? "Yes" : "No"}
                                  </span>
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </table>
                    )}
                  </div>
                </div>
                <hr />
                <h4>Data not received</h4>
                <h6>
                  Subscribed attributes that do not provide data from TangoGQL
                </h6>
                <div className="metrics-wrapper">
                  <div
                    id="metrics-view"
                    className={`table-responsive table-responsive table-sm `}
                  >
                    {devicesWithListenersNotRunning.length === 0 && (
                      <div className="font-italic ml-3">
                        No subscribed attribute is inaccessible from TangoGQL
                      </div>
                    )}
                    {devicesWithListenersNotRunning.length > 0 && (
                      <table
                        className={`table table-bordered  table-hover align-items-center justify-content-center"`}
                      >
                        <tbody>
                          <tr className="thead-light">
                            <th>Attribute</th>
                            <th>Subscription type</th>
                            <th>Listeners</th>
                            <th>Pub/Sub</th>
                            <th>Device Accessible</th>
                          </tr>
                          {devicesWithListenersNotRunning.map((element, i) => {
                            return (
                              <tr key={i}>
                                <td>{element.name}</td>
                                <td>{element.eventType}</td>
                                <td>{element.listeners}</td>
                                <td>{String(pubSub)}</td>
                                <td>
                                  <span
                                    style={{
                                      backgroundColor: "red",
                                      padding: "3px",
                                      borderRadius: "2px",
                                      color: "white",
                                    }}
                                  >
                                    {element.deviceAccessible ? "Yes" : "No"}
                                  </span>
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </table>
                    )}
                  </div>
                </div>
                {invalidAttribute.length !== 0 && (
                  <div>
                    <hr />
                    <h4>Attributes not registered</h4>
                    <h6>
                      These attributes are not registered in Tango. You can see
                      these devices marked with a warning label on the dashboard
                      in edit mode.
                    </h6>
                    <div className="metrics-wrapper">
                      <div
                        id="metrics-view"
                        className={`table-responsive table-responsive table-sm `}
                      >
                        {invalidAttribute.length > 0 && (
                          <div>
                            {invalidAttribute.map((element, i) => {
                              return (
                                <span key={i} style={tagStyle}>
                                  {element}
                                </span>
                              );
                            })}
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </Tab>
            <Tab
              eventKey="metrics"
              title="Metrics"
              disabled={"true" === sessionStorage.getItem("TANGOGQL_ARIADNE")}
            >
              <h4>Time when the last queries were executed on TangoGQL</h4>
              <div className="metrics-wrapper">
                <div
                  id="metrics-view"
                  className={`table-responsive table-responsive table-sm `}
                >
                  {query.length === 0 && (
                    <div className="font-italic ml-3">
                      No metrics about query available from TangoGQL
                    </div>
                  )}
                  {query.length > 0 && (
                    <table
                      className={`table table-bordered  table-hover align-items-center justify-content-center"`}
                    >
                      <tbody>
                        <tr className="thead-light">
                          <th>Communication</th>
                          <th>Time (ms)</th>
                        </tr>
                        {query.map((element, i) => {
                          return (
                            <tr key={i}>
                              <td>{element.argument}</td>
                              <td>{element.duration}</td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  )}
                </div>
              </div>
              <hr />
              <h4>Time when the last mutations were executed on TangoGQL</h4>
              <div className="metrics-wrapper">
                <div
                  id="metrics-view"
                  className={`table-responsive table-responsive table-sm `}
                >
                  {mutation.length === 0 && (
                    <div className="font-italic ml-3">
                      No metrics about mutation available from TangoGQL
                    </div>
                  )}
                  {mutation.length > 0 && (
                    <table
                      className={`table table-bordered  table-hover align-items-center justify-content-center"`}
                    >
                      <tbody>
                        <tr className="thead-light">
                          <th>Communication</th>
                          <th>Time (ms)</th>
                        </tr>
                        {mutation
                          .filter((item) => item.argument !== "{}")
                          .map((element, i) => {
                            return (
                              <tr key={i}>
                                <td>{element.argument}</td>
                                <td>{element.duration}</td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </table>
                  )}
                </div>
              </div>
            </Tab>
          </Tabs>
        )}
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={(e) => setState({ showModal: false })}
        >
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default MetricsModal;
