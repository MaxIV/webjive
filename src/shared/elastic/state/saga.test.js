import { fetchElasticLogsSaga } from './saga';

const stepper = (fn) => (mock) => fn.next(mock).value

test('the fetchElasticLogs saga happy path', async () => {

    const step = stepper(fetchElasticLogsSaga());
    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('FORK');
    expect(SageStep1take.payload.args[0]).toEqual('FETCH_ELASTIC');
});

test('the fetchElasticLogs saga sad path', async () => {

    const step = stepper(fetchElasticLogsSaga());
    const SageStep1take = step();
    expect(SageStep1take.type).toEqual('FORK');
});
