import {
  FETCH_ALARM_INDICATOR_DEVICES_SUCCESS,
} from "../actions/actionTypes";
import JiveAction from "../actions";

export interface IAlarmIndicatorDeviceState {
  deviceList: string[];
}

const initialState: IAlarmIndicatorDeviceState = {
  deviceList: []
};

export default function alarmIndicatorDeviceList(
  state: IAlarmIndicatorDeviceState = initialState,
  action: JiveAction
): IAlarmIndicatorDeviceState {
  switch (action.type) {
    case FETCH_ALARM_INDICATOR_DEVICES_SUCCESS: {
      return {
        ...state,
       deviceList: action.devices,  
      };
    }
    default:
      return state;
  }
}
