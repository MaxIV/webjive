import { createSelector } from "reselect";
import { IRootState } from "../reducers/rootReducer";

function getDatabaseState(state: IRootState) {
  return state.database;
}

function getTangoHostState(state: IRootState) {
  return state.tangoHost;
}

export const getInfo = createSelector(
  getDatabaseState,
  state => state.info
);


export const getTangoDBName = createSelector(
  getDatabaseState,
  state => state.tangoDBName
);

export const getTangoHost = createSelector(
  getTangoHostState,
  state => state.data
);
