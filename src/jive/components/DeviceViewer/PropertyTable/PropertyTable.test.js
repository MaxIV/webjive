import React from 'react';
import { configure, mount } from 'enzyme';
import { isMobile } from 'react-device-detect';
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import PropertyTable from './PropertyTable';
import Adapter from "@cfaester/enzyme-adapter-react-18";

configure({ adapter: new Adapter() });

const mockStore = configureStore([]);

jest.mock('react-device-detect', () => ({
  isMobile: jest.fn()
}));


describe('PropertyTable', () => {

  let wrapper;

  beforeEach(() => {

    const store = mockStore({
      user: {
        username: 'user1',
      },
      messages: {
        tg_test: {
          attributes: {
            attribute1: {
              values: [null],
              timestamp: [null]
            }
          }
        }
      }
    });

    isMobile.mockImplementation(() => true);
    const props = {
      deviceName: 'TestDevice',
      properties: [{ name: 'polled_attr', value: ['example value 1'] }],
      isLoggedIn: true,
      onSubscribe: jest.fn(),
      onAddProperty: jest.fn(),
      onEditProperty: jest.fn(),
      onDeleteProperty: jest.fn()
    };

    wrapper = mount(
      <Provider store={store}>
        <PropertyTable {...props} />
      </Provider>
    );
  });

  it("should render polled_attr and example value 1 if properties are not null", () => {

    expect(wrapper.html()).toContain('polled_attr');
    expect(wrapper.html()).toContain('example value 1');
  });

  it('opens the add property modal', () => {
    wrapper.find('.fa-plus').simulate('click');
    expect(wrapper.html()).toContain('example value 1');
  });

  it('opens the delete property modal', () => {
    wrapper.find('.fa-trash').simulate('click');
    expect(wrapper.html()).toContain('example value 1');
  });

  it('opens the edit property modal', () => {
    wrapper.find('.fa-pencil').simulate('click');
    expect(wrapper.html()).toContain('example value 1');
  });

  it("Should trigger onClose when add property modal is open", () => {
    wrapper.find('.fa-plus').simulate('click');
    wrapper.find('.btn-outline-secondary').at(0).simulate('click');
    expect(wrapper.html()).toContain('example value 1');
  });

  it("Should trigger onClose when edit property modal is open", () => {
    wrapper.find('.fa-pencil').simulate('click');
    wrapper.find('.btn-outline-secondary').at(0).simulate('click');
    expect(wrapper.html()).toContain('example value 1');
  });

  it("Should trigger onClose when delete property modal is open", () => {
    wrapper.find('.fa-trash').simulate('click');
    wrapper.find('.btn-outline-secondary').at(0).simulate('click');
    expect(wrapper.html()).toContain('example value 1');
  });

  it("Should trigger submit when add property modal is open", () => {
    wrapper.find('.fa-plus').simulate('click');
    wrapper
      .find("input")
      .simulate("change", { target: { value: "new value" } });
    wrapper.find('.btn-primary').at(0).simulate('click');
    expect(wrapper.html()).toContain('example value 1');
  });

  it("Should trigger submit when edit property modal is open", () => {
    wrapper.find('.fa-pencil').simulate('click');
    wrapper.find('.btn-primary').at(0).simulate('click');
    expect(wrapper.html()).toContain('example value 1');
  });

  it("Should trigger submit when delete property modal is open", () => {
    wrapper.find('.fa-trash').simulate('click');
    wrapper.find('.btn-primary').at(0).simulate('click');
    expect(wrapper.html()).toContain('example value 1');
  });

});
