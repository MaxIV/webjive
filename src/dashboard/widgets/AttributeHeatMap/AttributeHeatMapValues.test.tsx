import React from "react";
import { render } from "@testing-library/react";
import { useSelector as useSelectorMock } from "react-redux";
import AttributeHeatMapValues from "./AttributeHeatMapValues";
import {
  getAttributeLastValueFromState as getAttributeLastValueFromStateMock,
} from "../../../shared/utils/getLastValueHelper";
import { AttributeInput } from "../../types";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  connect: () => (Component: any) => Component,
}));
const getAttributeLastValueFromState = getAttributeLastValueFromStateMock as jest.Mock;

jest.mock("../../../shared/utils/getLastValueHelper", () => ({
  getAttributeLastValueFromState: jest.fn(),
  getAttributeLastTimeStampFromState: jest.fn(),
  getAttributeLastQualityFromState: jest.fn(),
}));

const useSelector = useSelectorMock as jest.Mock;

describe("AttributeHeatMapValues", () => {
  let myAttributeInput: AttributeInput;
  let xAxisInput: AttributeInput, yAxisInput: AttributeInput;
  var writeArray: any = [];
  var date = new Date();

  beforeEach(() => {
    useSelector.mockImplementation((selectorFn) =>
      selectorFn({
        messages: {},
        ui: {
          mode: "run",
        },
      })
    );

    xAxisInput = {
      device: "sys/tg_test/x",
      attribute: "x-axis",
      label: "x-axis",
      isNumeric: true,
      write: writeArray,
      writeValue: [0],
      value: [1, 2, 3],
      timestamp: date.getTime(),
      history: [],
      dataFormat: "spectrum",
      dataType: "DevDouble",
      enumlabels: [],
      unit: "",
      quality: "VALID",
    };

    yAxisInput = {
      device: "sys/tg_test/y",
      attribute: "y-axis",
      label: "y-axis",
      isNumeric: true,
      write: writeArray,
      writeValue: [0],
      value: [1, 2, 3],
      timestamp: date.getTime(),
      history: [],
      dataFormat: "spectrum",
      dataType: "DevDouble",
      enumlabels: [],
      unit: "",
      quality: "VALID",
    };

    myAttributeInput = {
      device: "sys/tg_test/Input",
      attribute: "Image",
      label: "Image",
      isNumeric: true,
      write: writeArray,
      writeValue: [0],
      value: [
        [30, 60, 1],
        [20, 1, 60],
        [1, 20, 30],
      ],
      timestamp: date.getTime(),
      history: [],
      dataFormat: "spectrum",
      dataType: "DevDouble",
      enumlabels: [],
      unit: "",
      quality: "VALID",
    };
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it("renders with run mode", () => {
    const { container } = render(
      <AttributeHeatMapValues
        setState={jest.fn()}
        mode={"run"}
        attribute={myAttributeInput}
        xAxis={xAxisInput}
        yAxis={yAxisInput}
        fixedScale={true}
        minValue={1}
        maxValue={30}
        defaultMinValue={1}
        defaultMaxValue={30}
        selectAxisAttribute={false}
        layout={jest.fn()}
        config={{ staticPlot: true }}
        responsive={true}
        style={{
          width: 100,
          height: 150,
        }}
        onAfterPlot={jest.fn()}
        selectColorscale="Jet"
      />
    );
    expect(container.innerHTML).toContain("height: 150px");
  });

  it("renders with library mode", () => {
    const { container } = render(
      <AttributeHeatMapValues
        setState={jest.fn()}
        mode={"library"}
        attribute={myAttributeInput}
        xAxis={xAxisInput}
        yAxis={yAxisInput}
        fixedScale={true}
        minValue={1}
        maxValue={30}
        defaultMinValue={1}
        defaultMaxValue={30}        
        selectAxisAttribute={false}
        layout={jest.fn()}
        config={{ staticPlot: true }}
        responsive={true}
        style={{
          width: 100,
          height: 150,
        }}
        onAfterPlot={jest.fn()}
        selectColorscale="Jet"
      />
    );
    expect(container.innerHTML).toContain("height: 150px");
  });

  it("updates state when heatmapInvalid is true", () => {
    getAttributeLastValueFromState.mockImplementation(
      (state, device, attribute) => {
        if (device === "sys/tg_test/x" && attribute === "x-axis")
          return [1, 2, 3];
        if (device === "sys/tg_test/y" && attribute === "y-axis")
          return [4, 5, 6];
        return "1"; // Default return value
      }
    );

    const mockSetState = jest.fn();
    render(
      <AttributeHeatMapValues
        setState={mockSetState}
        mode={"run"}
        attribute={myAttributeInput}
        xAxis={xAxisInput}
        yAxis={yAxisInput}
        fixedScale={true}
        minValue={1}
        maxValue={30}
        defaultMinValue={1}
        defaultMaxValue={30}        
        selectAxisAttribute={true}
        layout={jest.fn()}
        config={{ staticPlot: true }}
        responsive={true}
        style={{
          width: 100,
          height: 150,
        }}
        onAfterPlot={jest.fn()}
        selectColorscale="Jet"
      />
    );
    expect(mockSetState).toHaveBeenCalledWith({ heatmapInvalid: true });
  });

  it("updates state when heatmapInvalid is false", () => {
    // Update values to make heatmapInvalid false
    getAttributeLastValueFromState.mockImplementation(
      (state, device, attribute) => {
        if (device === "sys/tg_test/x" && attribute === "x-axis")
          return [1, 2, 3];
        if (device === "sys/tg_test/y" && attribute === "y-axis") return null;
        return "1"; // Default return value
      }
    );

    // 1. Mock setState
    const mockSetState = jest.fn();

    // 2. Render the Component
    render(
      <AttributeHeatMapValues
        setState={mockSetState}
        mode={"run"}
        attribute={myAttributeInput}
        xAxis={xAxisInput}
        yAxis={yAxisInput}
        fixedScale={true}
        minValue={1}
        maxValue={30}
        defaultMinValue={1}
        defaultMaxValue={30}        
        selectAxisAttribute={true}
        layout={jest.fn()}
        config={{ staticPlot: true }}
        responsive={true}
        style={{
          width: 100,
          height: 150,
        }}
        onAfterPlot={jest.fn()}
        selectColorscale="Jet"
      />
    );

    expect(mockSetState).toHaveBeenCalledWith({ heatmapInvalid: false });
  });
});
