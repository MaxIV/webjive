import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import PlaceHolder from './PlaceHolder';

describe('PlaceHolder Widget', () => {
  test('renders with expected text', () => {
    render(<PlaceHolder.component />);
    expect(screen.getByText('Widget no longer available')).toBeInTheDocument();
  });
});
