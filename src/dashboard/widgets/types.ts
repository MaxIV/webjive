import {
  NumberInputDefinition,
  StringInputDefinition,
  DeviceInputDefinition,
  DeviceComplexInput,
  AttributeInputDefinition,
  AttributeComplexInput,
  CommandInputDefinition,
  CommandInputDefinitionMultiple,
  SelectInputDefinition,
  VariableInputDefinition,
  FileInputDefinition,
  ColorInputDefinition,
  ComplexInputDefinition,
  BooleanInputDefinition,
  CommandInput,
  CommandInputMultiple,
  AttributeInput,
  DeviceInput,
  StyleInputDefinition,
  DatePickerDefinition,
  MultipleSelectionInputDefinition,
} from "../types";

// This type mapping makes an assumption that technically isn't in line with
// the current design/docs: that _only_ complex inputs are repeated, and that
// _all_ complex inputs are repeated. However, I think this makes sense for
// almost all scenarios, and that it should be the general behaviour.

export type TypedInputs<T> = {
  [K in keyof T]: T[K] extends NumberInputDefinition
    ? number
    : T[K] extends StringInputDefinition
    ? string
    : T[K] extends StyleInputDefinition
    ? string
    : T[K] extends BooleanInputDefinition
    ? boolean
    : T[K] extends DeviceInputDefinition
    ? DeviceInput
    : T[K] extends DeviceComplexInput
    ? DeviceComplexInput
    : T[K] extends AttributeInputDefinition
    ? AttributeInput
    : T[K] extends AttributeComplexInput
    ? AttributeComplexInput
    : T[K] extends CommandInputDefinition
    ? CommandInput
    : T[K] extends CommandInputDefinitionMultiple
    ? CommandInputMultiple
    : T[K] extends SelectInputDefinition
    ? string
    : T[K] extends MultipleSelectionInputDefinition<infer U>
    ? U
    : T[K] extends VariableInputDefinition<infer U>
    ? U
    : T[K] extends FileInputDefinition
    ? FileInputDefinition
    : T[K] extends ColorInputDefinition
    ? string
    : T[K] extends ComplexInputDefinition<infer U>
    ? Array<TypedInputs<U>> // Assumes that complex inputs are always repeated
    : T[K] extends DatePickerDefinition
    ? Date
    : never;
};

export type WidgetProps<T> = {
  inputs: TypedInputs<T>;
  t0: number;
  mode: "run" | "edit" | "library";
  actualWidth: number;
  actualHeight: number;
  id: number;
};
