import React from "react";
import { render } from "@testing-library/react";
import { useSelector as useSelectorMock } from "react-redux";
import AttributeScatterValues from "./AttributeScatterValues";
import {
  getAttributeLastTimeStampFromState as getAttributeLastTimeStampFromStateMock,
  getAttributeLastValueFromState as getAttributeLastValueFromStateMock,
  getAttributeLastQualityFromState as getAttributeLastQualityFromStateMock,
} from "../../../shared/utils/getLastValueHelper";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  connect: () => (Component: any) => Component,
}));

const getAttributeLastTimeStampFromState = getAttributeLastTimeStampFromStateMock as jest.Mock;
const getAttributeLastValueFromState = getAttributeLastValueFromStateMock as jest.Mock;
const getAttributeLastQualityFromState = getAttributeLastQualityFromStateMock as jest.Mock;

jest.mock("../../../shared/utils/getLastValueHelper", () => ({
  getAttributeLastValueFromState: jest.fn(),
  getAttributeLastTimeStampFromState: jest.fn(),
  getAttributeLastQualityFromState: jest.fn(),
}));

const useSelector = useSelectorMock as jest.Mock;

describe("AttributeScatterValues", () => {
  beforeEach(() => {
    useSelector.mockImplementation((selectorFn) =>
      selectorFn({
        messages: {},
        ui: {
          mode: "run",
        },
      })
    );
    getAttributeLastValueFromState.mockReturnValue("1");
    getAttributeLastTimeStampFromState.mockReturnValue(Date.now() / 1000);
    getAttributeLastQualityFromState.mockReturnValue("ATTR_VALID");
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  let dependentAttribute = {
      device: "sys/tg_test/1",
      attribute: "time_range",
      label: "TimeRange",
      isNumeric: true,
      value: -1,
      history: [
        {
          timestamp: 1,
          value: 1,
          writeValue: "",
          quality: "",
        },
      ],
      dataType: "",
      dataFormat: "",
      enumlabels: [],
      unit: "",
      write: function(value: any): void {
        throw new Error("Function not implemented.");
      },
      writeValue: undefined,
      quality: "",
      timestamp: 0,
    },
    independentAttribute = {
      device: "sys/tg_test/1",
      attribute: "time_range",
      label: "TimeRange",
      isNumeric: true,
      value: -1,
      history: [
        {
          timestamp: 1,
          value: 1,
          writeValue: "",
          quality: "",
        },
      ],
      dataType: "",
      dataFormat: "",
      enumlabels: [],
      unit: "",
      write: function(value: any): void {
        throw new Error("Function not implemented.");
      },
      writeValue: undefined,
      quality: "",
      timestamp: 0,
    };

  it("renders without crashing when mode is 'library'", () => {
    const { container } = render(
      <AttributeScatterValues
        dependent={dependentAttribute}
        independent={independentAttribute}
        mode="library"
        layout={{}}
        config={{ staticPlot: true }}
        responsive={true}
        style={{ height: "150px" }}
      />
    );
    expect(container.innerHTML).toContain("height: 150px");
  });

  it("renders without crashing when mode is 'run'", () => {
    const { container } = render(
      <AttributeScatterValues
        dependent={dependentAttribute}
        independent={independentAttribute}
        mode="run"
        layout={{}}
        config={{ staticPlot: true }}
        responsive={true}
        style={{ height: "150px" }}
      />
    );
    expect(container.innerHTML).toContain("height: 150px");
  });

  it("renders without crashing when mode is 'edit'", () => {
    const { container } = render(
      <AttributeScatterValues
        dependent={dependentAttribute}
        independent={independentAttribute}
        mode="run"
        layout={{}}
        config={{ staticPlot: true }}
        responsive={true}
        style={{ height: "150px" }}
      />
    );
    expect(container.innerHTML).toContain("height: 150px");
  });
});
