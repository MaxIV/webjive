import React from "react";
import { render, act } from "@testing-library/react";
import { useSelector as useSelectorMock } from "react-redux";
import SpectrumValues from "./SpectrumValues";
import {
  getAttributeLastValueFromState as getAttributeLastValueFromStateMock,
} from "../../../shared/utils/getLastValueHelper";
import "@testing-library/jest-dom";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
}));

const getAttributeLastValueFromState = getAttributeLastValueFromStateMock as jest.Mock;

jest.mock("../../../shared/utils/getLastValueHelper", () => ({
  getAttributeLastValueFromState: jest.fn(),
}));

const useSelector = useSelectorMock as jest.Mock;

describe("SpectrumValues", () => {
  beforeEach(() => {
    useSelector.mockImplementation((selectorFn: any) =>
      selectorFn({
        messages: {
          "sys/tg_test/1": {
            attributes: {
              boolean_scalar: {
                values: [true, true, true],
                quality: ["ATTR_VALID", "ATTR_VALID", "ATTR_VALID"],
                timestamp: [
                  1689077614.017916,
                  1689077614.061947,
                  1689077674.123604,
                ],
              },
            },
          },
        },
        ui: {
          mode: "run",
        },
      })
    );
    getAttributeLastValueFromState.mockReturnValue([1, 2, 3]);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it("should render the div containing plotly", () => {
    const { container } = render(
      <SpectrumValues
        deviceName="sys/tg_test/1"
        attributeName="double_spectrum_ro"
        mode="edit"
        title="sys/tg_test/1/double_spectrum_ro"
        titlefont={{ size: 12 }}
        font={{ family: "Helvetica, Arial, sans-serif" }}
        margin={{ l: 30, r: 15, t: 35, b: 20 }}
        autosize={true}
        timeWindow={120}
        inelastic={true}
        config={{ staticPlot: true }}
        responsive={true}
        style={{ width: 599, height: 407 }}
        showSpecificYIndexValue="1"
        showSpecificXIndexValue="0" // Ensure showSpecificXIndexValue is passed
        backgroundColor=""
        lineColor=""
        xaxis=""
        yaxis=""
      />
    );

    expect(container.querySelector("#plotlySpectrumDiv")).toBeInTheDocument();
  });

  it("checks width and height", () => {
    const { container } = render(
      <SpectrumValues
        deviceName="sys/tg_test/1"
        attributeName="double_spectrum_ro"
        mode="edit"
        title="sys/tg_test/1/double_spectrum_ro"
        titlefont={{ size: 12 }}
        font={{ family: "Helvetica, Arial, sans-serif" }}
        margin={{ l: 30, r: 15, t: 35, b: 20 }}
        autosize={true}
        timeWindow={120}
        inelastic={true}
        config={{ staticPlot: true }}
        responsive={true}
        style={{ width: 599, height: 407 }}
        showSpecificYIndexValue="1"
        showSpecificXIndexValue="0" // Ensure showSpecificXIndexValue is passed
        backgroundColor=""
        lineColor=""
        xaxis=""
        yaxis=""
      />
    );

    const outerDiv = container.querySelector("#plotlySpectrumDiv");

    // Check if the outer div exists
    expect(outerDiv).toBeInTheDocument();

    if (outerDiv) {
      // Find the inner div with the applied styles
      const innerDiv = outerDiv.querySelector("div");

      // Check if the inner div exists and has the correct styles
      expect(innerDiv).toBeInTheDocument();
      expect(innerDiv).toHaveStyle("width: 599px");
      expect(innerDiv).toHaveStyle("height: 407px");
    }
  });

  it("should return empty yAxis when inelastic is false", () => {
    const { container } = render(
      <SpectrumValues
        deviceName="sys/tg_test/1"
        attributeName="double_spectrum_ro"
        mode="edit"
        title="Test Plot"
        titlefont={{ size: 12 }}
        font={{ family: "Helvetica, Arial, sans-serif" }}
        margin={{ l: 30, r: 15, t: 35, b: 20 }}
        autosize={true}
        timeWindow={120}
        inelastic={false}
        config={{ staticPlot: true }}
        responsive={true}
        style={{ width: 600, height: 400 }}
        showSpecificYIndexValue="1"
        showSpecificXIndexValue="0"
        backgroundColor=""
        lineColor=""
        xaxis=""
        yaxis=""
      />
    );

    const plotlyDiv = container.querySelector("#plotlySpectrumDiv");
    expect(plotlyDiv).toBeInTheDocument();

    const plotlyComponent = plotlyDiv?.querySelector("div");
    expect(plotlyComponent).toBeInTheDocument();
  });

  it("should remove oldest value when history exceeds timeWindow", () => {
    window['config'].historyLimit = 2;
    let mockValue = [1, 2, 3];

    useSelector.mockImplementation((selectorFn: any) =>
      selectorFn({
        messages: {
          "sys/tg_test/1": {
            attributes: {
              double_spectrum_ro: {
                values: mockValue,
                quality: ["ATTR_VALID", "ATTR_VALID", "ATTR_VALID"],
                timestamp: [
                  1689077614.017916,
                  1689077614.061947,
                  1689077674.123604,
                ],
              },
            },
          },
        },
        ui: {
          mode: "run",
        },
      })
    );

    const { rerender, container } = render(
      <SpectrumValues
        deviceName="sys/tg_test/1"
        attributeName="double_spectrum_ro"
        mode="edit"
        title="Test Plot"
        titlefont={{ size: 12 }}
        font={{ family: "Helvetica, Arial, sans-serif" }}
        margin={{ l: 30, r: 15, t: 35, b: 20 }}
        autosize={true}
        timeWindow={20}
        inelastic={true}
        config={{ staticPlot: true }}
        responsive={true}
        style={{ width: 600, height: 400 }}
        showSpecificYIndexValue="1"
        showSpecificXIndexValue="0"
        backgroundColor=""
        lineColor=""
        xaxis=""
        yaxis=""
      />
    );

    for (let i = 0; i < 21; i++) {
      let mockValue = [i, i + 1, i + 2];
      act(() => {
        getAttributeLastValueFromState.mockReturnValue(mockValue);
        rerender(
          <SpectrumValues
            deviceName="sys/tg_test/1"
            attributeName="double_spectrum_ro"
            mode="edit"
            title="Test Plot"
            titlefont={{ size: 12 }}
            font={{ family: "Helvetica, Arial, sans-serif" }}
            margin={{ l: 30, r: 15, t: 35, b: 20 }}
            autosize={true}
            timeWindow={20}
            inelastic={true}
            config={{ staticPlot: true }}
            responsive={true}
            style={{ width: 600, height: 400 }}
            showSpecificYIndexValue="1"
            showSpecificXIndexValue="0"
            backgroundColor=""
            lineColor=""
            xaxis=""
            yaxis=""
          />
        );
      });
    }

    const plotlyDiv = container.querySelector("#plotlySpectrumDiv");
    expect(plotlyDiv).toBeInTheDocument();

    const plotlyComponent = plotlyDiv?.querySelector("div");
    expect(plotlyComponent).toBeInTheDocument();
  });
});
