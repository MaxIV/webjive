import React, { Component } from "react";
import { WidgetProps } from "../types";
import {
  WidgetDefinition,
  BooleanInputDefinition,
  AttributeInputDefinition,
  SelectInputDefinition,
  StringInputDefinition,
  NumberInputDefinition,
  ColorInputDefinition,
} from "../../types";
import SpectrumValues from "./SpectrumValues";
import { showHideTangoDBName } from "../../DBHelper";

type Inputs = {
  attribute: AttributeInputDefinition;
  showTitle: BooleanInputDefinition;
  showTangoDB: BooleanInputDefinition;
  inelastic: BooleanInputDefinition;
  showAttribute: SelectInputDefinition;
  showSpecificYIndexValue: StringInputDefinition;
  showSpecificXIndexValue?: StringInputDefinition;
  timeWindow: NumberInputDefinition;
  lineColor: ColorInputDefinition;
  textColor: ColorInputDefinition;
  backgroundColor: ColorInputDefinition;
  xScientificNotation: BooleanInputDefinition;
  yScientificNotation: BooleanInputDefinition;
  xLogarithmicScale: BooleanInputDefinition;
  yLogarithmicScale: BooleanInputDefinition;
};

type Props = WidgetProps<Inputs>;

class Spectrum extends Component<Props> {
  public constructor(props: Props) {
    super(props);
  }

  public render() {
    const { mode, inputs } = this.props;
    const { 
      attribute, 
      showTitle, 
      inelastic, 
      showAttribute, 
      xScientificNotation,
      yScientificNotation,
      xLogarithmicScale,
      yLogarithmicScale,
      showTangoDB, 
      showSpecificYIndexValue, 
      showSpecificXIndexValue, 
      timeWindow,
      lineColor,
      textColor,
      backgroundColor,
    } = inputs;
    let display = "";
    const deviceName = showHideTangoDBName(true, showTangoDB, attribute.device);
    if (showAttribute === "Label") display = attribute.label;
    else if (showAttribute === "Name") display = attribute.attribute;
    const title =
      showTitle === false
        ? null
        : mode === "library"
          ? "device/attribute"
          : `${deviceName || "?"}/${display || "?"}`;

    return (
      <div>
        <SpectrumValues
          deviceName={attribute.device}
          attributeName={attribute.attribute}
          mode={mode}
          title={title}
          titlefont={{ size: 12 }}
          font={{ family: "Helvetica, Arial, sans-serif", color: textColor }}
          backgroundColor={backgroundColor}
          lineColor={lineColor}
          margin={{
            l: 30,
            r: 15,
            t: 15 + (showTitle ? 20 : 0),
            b: 20,
          }}
          autosize={true}
          timeWindow={timeWindow}
          inelastic={inelastic}
          config={{ staticPlot: mode === "run" ? false : true }}
          responsive={true}
          style={{
            width: this.props.actualWidth,
            height: mode === "library" ? 150 : this.props.actualHeight,
          }}
          showSpecificYIndexValue={showSpecificYIndexValue}
          showSpecificXIndexValue={showSpecificXIndexValue} // Pass new prop
          xaxis={{
            type: xLogarithmicScale ? "log" : "linear",
            exponentformat: xScientificNotation ? "e" : "none"
          }}
          yaxis={{
            type: yLogarithmicScale ? "log" : "linear",
            exponentformat: yScientificNotation ? "e" : "none"
          }}
        />
      </div>
    );
  }
}

const definition: WidgetDefinition<Inputs> = {
  type: "SPECTRUM",
  name: "Spectrum",
  defaultWidth: 30,
  defaultHeight: 20,
  inputs: {
    attribute: {
      label: "",
      type: "attribute",
      dataFormat: "spectrum",
      dataType: "numeric",
      required: true,
    },
    showSpecificYIndexValue: {
      type: "string",
      label: "y-axis index",
      placeholder: "Type the Y index",
    },
    showSpecificXIndexValue: {
      type: "string",
      label: "x-axis index",
      placeholder: "Type the X index",
    },
    timeWindow: {
      type: "number",
      default: 120,
      label: "Time Window"
    },
    showAttribute: {
      type: "select",
      label: "Attribute display:",
      default: "Label",
      options: [
        {
          name: "Label",
          value: "Label",
        },
        {
          name: "Name",
          value: "Name",
        },
      ],
    },
    showTitle: {
      type: "boolean",
      label: "Show Title",
      default: true,
    },
    showTangoDB: {
      type: "boolean",
      label: "Show Tango database name",
      default: false,
    },
    xScientificNotation: {
      type: "boolean",
      label: "X-axis Scientific Notation",
      default: false,
    },
    yScientificNotation: {
      type: "boolean",
      label: "Y-axis Scientific Notation",
      default: false,
    },
    xLogarithmicScale: {
      type: "boolean",
      label: "X-axis Logarithmic Scale",
      default: false,
    },
    yLogarithmicScale: {
      type: "boolean",
      label: "Y-axis Logarithmic Scale",
      default: false,
    },
    inelastic: {
      type: "boolean",
      label: "Inelastic Y Axis",
      default: false,
    },
    lineColor: {
      label: "Line Color",
      type: "color",
      default: "#1f77b4"
    },
    textColor: {
      label: "Text Color",
      type: "color",
      default: "#000000",
    },
    backgroundColor: {
      label: "Background Color",
      type: "color",
      default: "#ffffff",
    },
  },
};

const SpectrumExport = { component: Spectrum, definition };
export default SpectrumExport;
