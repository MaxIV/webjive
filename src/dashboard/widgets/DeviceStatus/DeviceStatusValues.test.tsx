import React from "react";
import { render } from "@testing-library/react";
import { useSelector as useSelectorMock } from "react-redux";
import {
  getAttributeLastTimeStampFromState as getAttributeLastTimeStampFromStateMock,
  getAttributeLastValueFromState as getAttributeLastValueFromStateMock,
} from "../../../shared/utils/getLastValueHelper";
import DeviceStatusValues from "./DeviceStatusValues";

jest.mock("react-redux", () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn(),
}));

const getAttributeLastTimeStampFromState = getAttributeLastTimeStampFromStateMock as jest.Mock;
const getAttributeLastValueFromState = getAttributeLastValueFromStateMock as jest.Mock;

jest.mock("../../../shared/utils/getLastValueHelper", () => ({
  getAttributeLastValueFromState: jest.fn(),
  getAttributeLastTimeStampFromState: jest.fn(),
}));

const useSelector = useSelectorMock as jest.Mock;

describe("AttributeValues", () => {
  beforeEach(() => {
    useSelector.mockImplementation((selectorFn: any) =>
      selectorFn({
        messages: {},
        ui: {
          mode: "run",
        },
      })
    );
    getAttributeLastTimeStampFromState.mockReturnValue(Date.now() / 1000);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it("renders with RUNNING", () => {
    getAttributeLastValueFromState.mockReturnValue("RUNNING");

    const { container } = render(
      <DeviceStatusValues
        device="tangodb://sys/tg_test/1"
        showDeviceName={true}
        showTangoDB={false}
        showStateString={true}
        showStateLED={true}
        LEDSize={1}
        textColor="darkgreen"
        backgroundColor="black"
        textSize={3}
        linkTo=""
        alignValueRight={false}
      />
    );
    expect(container.innerHTML).toContain("sys/tg_test/1");
    expect(container.innerHTML).toContain("RUNNING");
    expect(container.innerHTML).toContain("height: 1em");
    expect(container.innerHTML).toContain("font-size: 3em");
    expect(container.innerHTML).toContain("darkgreen");
  });

  it("renders with tangodb name", () => {
    getAttributeLastValueFromState.mockReturnValue("RUNNING");

    const { container } = render(
      <DeviceStatusValues
        device="tangodb://sys/tg_test/1"
        showDeviceName={true}
        showTangoDB={true}
        showStateString={true}
        showStateLED={true}
        LEDSize={1}
        textColor="darkgreen"
        backgroundColor="black"
        textSize={3}
        linkTo=""
        alignValueRight={false}
      />
    );
    expect(container.innerHTML).toContain("tangodb://sys/tg_test/1");
    expect(container.innerHTML).toContain("RUNNING");
    expect(container.innerHTML).toContain("height: 1em");
    expect(container.innerHTML).toContain("font-size: 3em");
    expect(container.innerHTML).toContain("darkgreen");
  });

  it("renders with ON", () => {
    getAttributeLastValueFromState.mockReturnValue("ON");

    const { container } = render(
      <DeviceStatusValues
        device="tangodb://sys/tg_test/1"
        showDeviceName={true}
        showTangoDB={false}
        showStateString={true}
        showStateLED={true}
        LEDSize={1}
        textColor="green"
        backgroundColor="black"
        textSize={3}
        linkTo=""
        alignValueRight={false}
      />
    );
    expect(container.innerHTML).toContain("ON");
    expect(container.innerHTML).toContain("green");
  });

  it("renders with OFF", () => {
    getAttributeLastValueFromState.mockReturnValue("OFF");

    const { container } = render(
      <DeviceStatusValues
        device="tangodb://sys/tg_test/1"
        showDeviceName={true}
        showTangoDB={false}
        showStateString={true}
        showStateLED={true}
        LEDSize={1}
        textColor="white"
        backgroundColor="black"
        textSize={3}
        linkTo=""
        alignValueRight={false}
      />
    );
    expect(container.innerHTML).toContain("OFF");
    expect(container.innerHTML).toContain("white");
  });
  it("renders with MOVING", () => {
    getAttributeLastValueFromState.mockReturnValue("MOVING");

    const { container } = render(
      <DeviceStatusValues
        device="tangodb://sys/tg_test/1"
        showDeviceName={true}
        showTangoDB={false}
        showStateString={true}
        showStateLED={true}
        LEDSize={1}
        textColor="lightblue"
        backgroundColor="black"
        textSize={3}
        linkTo=""
        alignValueRight={false}
      />
    );
    expect(container.innerHTML).toContain("MOVING");
    expect(container.innerHTML).toContain("lightblue");
  });

  it("renders with STANDBY", () => {
    getAttributeLastValueFromState.mockReturnValue("STANDBY");

    const { container } = render(
      <DeviceStatusValues
        device="tangodb://sys/tg_test/1"
        showDeviceName={true}
        showTangoDB={false}
        showStateString={true}
        showStateLED={true}
        LEDSize={1}
        textColor="yellow"
        backgroundColor="black"
        textSize={3}
        linkTo=""
        alignValueRight={false}
      />
    );
    expect(container.innerHTML).toContain("STANDBY");
    expect(container.innerHTML).toContain("yellow");
  });

  it("renders with FAULT", () => {
    getAttributeLastValueFromState.mockReturnValue("FAULT");

    const { container } = render(
      <DeviceStatusValues
        device="tangodb://sys/tg_test/1"
        showDeviceName={true}
        showTangoDB={false}
        showStateString={true}
        showStateLED={true}
        LEDSize={1}
        textColor="red"
        backgroundColor="black"
        textSize={3}
        linkTo=""
        alignValueRight={false}
      />
    );
    expect(container.innerHTML).toContain("FAULT");
    expect(container.innerHTML).toContain("red");
  });

  it("renders with DISABLE", () => {
    getAttributeLastValueFromState.mockReturnValue("DISABLE");

    const { container } = render(
      <DeviceStatusValues
        device="tangodb://sys/tg_test/1"
        showDeviceName={true}
        showTangoDB={false}
        showStateString={true}
        showStateLED={true}
        LEDSize={1}
        textColor="magenta"
        backgroundColor="black"
        textSize={3}
        linkTo=""
        alignValueRight={false}
      />
    );
    expect(container.innerHTML).toContain("DISABLE");
    expect(container.innerHTML).toContain("magenta");
  });

  it("renders with INIT", () => {
    getAttributeLastValueFromState.mockReturnValue("INIT");

    const { container } = render(
      <DeviceStatusValues
        device="tangodb://sys/tg_test/1"
        showDeviceName={true}
        showTangoDB={false}
        showStateString={true}
        showStateLED={true}
        LEDSize={1}
        textColor="beige"
        backgroundColor="black"
        textSize={3}
        linkTo=""
        alignValueRight={false}
      />
    );
    expect(container.innerHTML).toContain("INIT");
    expect(container.innerHTML).toContain("beige");
  });

  it("renders with INIT, but unknown color", () => {
    getAttributeLastValueFromState.mockReturnValue("INIT");

    const { container } = render(
      <DeviceStatusValues
        device="tangodb://sys/tg_test/1"
        showDeviceName={true}
        showTangoDB={false}
        showStateString={true}
        showStateLED={true}
        LEDSize={1}
        textColor="unknown"
        backgroundColor="black"
        textSize={3}
        linkTo=""
        alignValueRight={false}
      />
    );
    expect(container.innerHTML).toContain("INIT");
    expect(container.innerHTML).not.toContain("unknown");
  });

  it("renders showStateLED = false", () => {
    getAttributeLastValueFromState.mockReturnValue("DNE");

    const { container } = render(
      <DeviceStatusValues
        device="tangodb://sys/tg_test/1"
        showDeviceName={true}
        showTangoDB={false}
        showStateString={true}
        showStateLED={false}
        LEDSize={1}
        textColor="pink"
        backgroundColor="black"
        textSize={3}
        linkTo=""
        alignValueRight={false}
      />
    );
    expect(container.innerHTML).toContain("DNE");
    expect(container.innerHTML).toContain("pink");
  });
});
