import React from "react";
import { DeviceInput, AttributeInput } from "../../types";

import { configure, mount } from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import DeviceStatus from "./DeviceStatus";

import { Provider } from "react-redux";
import configureStore from "../../../shared/state/store/configureStore";

configure({ adapter: new Adapter() });
const store = configureStore();

describe("DeviceStatus", () => {
  let myDeviceInput: DeviceInput;
  myDeviceInput = {
    alias: "",
    name: "sys/tg_test/1",
  };
  let myAttributeInput: AttributeInput;
  myAttributeInput = {
    attribute: "state",
    dataFormat: "scalar",
    dataType: "DevState",
    device: "",
    enumlabels: [],
    history: [],
    isNumeric: false,
    timestamp: 1,
    unit: "",
    write: () => {},
    value: "RUNNING",
    writeValue: null,
    quality: "VALID",
    label: "STATE",
  };
  it("renders without crashing", () => {
    const element = React.createElement(DeviceStatus.component, {
      mode: "run",
      t0: 1,
      actualWidth: 500,
      actualHeight: 140,
      inputs: {
        LEDSize: 1,
        showDeviceName: true,
        showTangoDB: false,
        showStateLED: true,
        showStateString: true,
        textSize: 3,
        device: myDeviceInput,
        state: myAttributeInput,
        alignValueRight: false,
        textColor: "red",
        backgroundColor: "black",
        linkTo: "",
        widgetCss: "",
      },
      id: 42,
    });

    let wrapper = mount(<Provider store={store}>{element}</Provider>);
    expect(wrapper.find("#DeviceStatus").exists()).toBe(true);
  });
});
