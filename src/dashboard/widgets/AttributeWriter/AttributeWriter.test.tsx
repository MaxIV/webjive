import React from "react";
import AttributeWriter from "./AttributeWriter";
import { Provider } from "react-redux";
import configureStore from "../../../shared/state/store/configureStore";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom";

const store = configureStore();
const renderComponent = (mode, inputs) => {
  return render(
    <Provider store={store}>
      <AttributeWriter.component
        mode={mode}
        t0={1}
        actualWidth={100}
        actualHeight={100}
        inputs={inputs}
        id={42}
      />
    </Provider>
  );
};

describe("AttributeWriter Display", () => {
  it("renders all true without crashing", () => {
    const inputs = {
      showDevice: true,
      showTangoDB: false,
      attribute: {
        device: "tangodb://sys/tg_test/1",
        attribute: "boolean_scalar",
        dataType: "DevBoolean",
        dataFormat: "scalar",
        isNumeric: false,
        unit: "",
        enumlabels: [],
        write: () => {},
        value: true,
        writeValue: "",
        label: "",
        history: [],
        timestamp: 0,
        quality: "VALID",
      },
      size: 1,
      showAttribute: "name",
      alignValueRight: true,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    renderComponent("run", inputs);
    const element = document.getElementById("AttributeWriter");
    expect(element).not.toBeNull();
  });

  it("renders not showing device name when 'showDevice' is false", () => {
    const attributeInput = {
      device: "tangodb://sys/tg_test/1",
      attribute: "boolean_scalar",
      dataType: "DevBoolean",
      dataFormat: "scalar",
      isNumeric: false,
      unit: "",
      enumlabels: [],
      write: () => {},
      value: true,
      writeValue: "",
      label: "AttributeWriter",
      history: [],
      timestamp: 0,
      quality: "VALID",
    };

    const inputs = {
      showDevice: false,
      showTangoDB: false,
      attribute: attributeInput,
      size: 1,
      showAttribute: "Label",
      alignValueRight: true,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    renderComponent("run", inputs);
    const deviceNameElement = screen.queryByText(attributeInput.device);
    expect(deviceNameElement).not.toBeInTheDocument();
  });

  it("renders showing tango db name when 'showTangoDB' is true", () => {
    const inputs = {
      showDevice: false,
      showTangoDB: true,
      attribute: {
        device: "tangodb://sys/tg_test/1",
        attribute: "boolean_scalar",
        dataType: "DevBoolean",
        dataFormat: "scalar",
        isNumeric: false,
        unit: "",
        enumlabels: [],
        write: () => {},
        value: true,
        writeValue: "",
        label: "AttributeWriter",
        history: [],
        timestamp: 0,
        quality: "VALID",
      },
      size: 1,
      showAttribute: "Label",
      alignValueRight: true,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    renderComponent("edit", inputs);

    const regex = /tangodb:\/\/AttributeWriter/i;
    const tangoDbText = screen.getByText(regex);
    expect(tangoDbText).toBeInTheDocument();
  });

  it("renders showing device name when 'showDevice' is true", () => {
    const inputs = {
      showDevice: true,
      showTangoDB: true,
      attribute: {
        device: "tangodb://sys/tg_test/1",
        attribute: "boolean_scalar",
        dataType: "DevBoolean",
        dataFormat: "scalar",
        isNumeric: false,
        unit: "",
        enumlabels: [],
        write: () => {},
        value: true,
        writeValue: "",
        label: "AttributeWriter",
        history: [],
        timestamp: 0,
        quality: "VALID",
      },
      size: 1,
      showAttribute: "Label",
      alignValueRight: true,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    renderComponent("edit", inputs);

    const regex = /tangodb:\/\/sys\/tg_test\/1/i;
    const tangoDbText = screen.getByText(regex);
    expect(tangoDbText).toBeInTheDocument();
  });

  it("render with DevEnum mode run with invalid value", () => {
    const inputs = {
      showDevice: true,
      showTangoDB: true,
      attribute: {
        device: "tangodb://sys/tg_test/1",
        attribute: "DevEnum",
        dataType: "DevEnum",
        dataFormat: "scalar",
        isNumeric: false,
        unit: "",
        enumlabels: [],
        write: () => {},
        value: "",
        writeValue: "",
        label: "label",
        history: [],
        timestamp: 0,
        quality: "VALID",
      },
      size: 1,
      showAttribute: "name",
      alignValueRight: true,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    renderComponent("run", inputs);

    const deviceNameElement = screen.getByText(/tangodb:\/\/sys\/tg_test\/1/i);
    expect(deviceNameElement).toBeInTheDocument();
  });

  it("render with unknown not implemented", () => {
    const inputs = {
      showDevice: false,
      showTangoDB: false,
      attribute: {
        device: "unknown",
        attribute: "unknown",
        dataType: "unknown",
        dataFormat: "scalar",
        isNumeric: false,
        unit: "",
        enumlabels: [],
        write: () => {},
        value: "",
        writeValue: "",
        label: "label",
        history: [],
        timestamp: 0,
        quality: "VALID",
      },
      size: 1,
      showAttribute: "Name",
      alignValueRight: true,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    renderComponent("run", inputs);
    const unknownMessage = screen.getByText(/unknown not implemented/i);
    expect(unknownMessage).toBeInTheDocument();
  });

  it("simulate focus", () => {
    const inputs = {
      showDevice: false,
      showTangoDB: false,
      attribute: {
        device: "tangodb://sys/tg_test/1",
        attribute: "DevString",
        dataType: "DevString",
        dataFormat: "scalar",
        isNumeric: false,
        unit: "",
        enumlabels: [],
        write: () => {},
        value: "stringSend",
        writeValue: "stringSend",
        label: "",
        history: [],
        timestamp: 0,
        quality: "VALID",
      },
      size: 1,
      showAttribute: "Label",
      alignValueRight: true,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    const { getByPlaceholderText } = renderComponent("run", inputs);

    const input = getByPlaceholderText("stringSend") as HTMLInputElement;

    fireEvent.change(input, { target: { value: "stringSend" } });
    fireEvent.focus(input);
    fireEvent.blur(input);
    expect(input.value).toEqual("stringSend");
  });

  it("submit on empty string", () => {
    const inputs = {
      showDevice: false,
      showTangoDB: false,
      attribute: {
        device: "tangodb://sys/tg_test/1",
        attribute: "DevString",
        dataType: "DevString",
        dataFormat: "scalar",
        isNumeric: false,
        unit: "",
        enumlabels: [],
        write: () => {},
        value: "stringSend",
        writeValue: "stringSend",
        label: "",
        history: [],
        timestamp: 0,
        quality: "VALID",
      },
      size: 1,
      showAttribute: "Label",
      alignValueRight: true,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    renderComponent("run", inputs);
    const input = screen.getByPlaceholderText("stringSend") as HTMLInputElement;
    fireEvent.change(input, { target: { value: "" } });
    fireEvent.submit(input);

    expect(screen.getByText("The input can't be empty")).toBeInTheDocument();
  });

  it("submit DevDouble", async () => {
    const inputs = {
      showDevice: false,
      showTangoDB: false,
      attribute: {
        device: "tangodb://sys/tg_test/1",
        attribute: "DevDouble",
        dataType: "DevDouble",
        dataFormat: "scalar",
        isNumeric: true,
        unit: "",
        enumlabels: [],
        write: () => {},
        value: 1,
        writeValue: 1,
        label: "",
        history: [],
        timestamp: 0,
        quality: "VALID",
      },
      size: 1,
      showAttribute: "Label",
      alignValueRight: true,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };
    renderComponent("run", inputs);

    const input = screen.getByPlaceholderText("1.00") as HTMLInputElement;
    fireEvent.change(input, { target: { value: "1.00" } });

    await waitFor(() => {
      expect(input.value).toBe("1.00");
    });
  });

  it("submit DevBoolean", async () => {
    const inputs = {
      showDevice: false,
      showTangoDB: false,
      showAttribute: "Label",
      alignValueRight: true,
      attribute: {
        device: "tangodb://sys/tg_test/1",
        attribute: "DevBoolean",
        dataType: "DevBoolean",
        dataFormat: "scalar",
        isNumeric: true,
        unit: "",
        enumlabels: [],
        write: () => {},
        value: 1,
        writeValue: 1,
        label: "",
        history: [],
        timestamp: 0,
        quality: "VALID",
      },
      size: 1,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    renderComponent("run", inputs);

    const input = screen.getByPlaceholderText("1") as HTMLInputElement;

    fireEvent.change(input, { target: { value: "0" } });
    fireEvent.change(input, { target: { value: "1" } });

    await waitFor(() => {
      expect(input.value).toBe("1");
    });
  });

  it("submit DevEnum with FAULT", async () => {
    const inputs = {
      showDevice: false,
      showTangoDB: false,
      showAttribute: "Label",
      alignValueRight: true,
      attribute: {
        device: "tangodb://sys/tg_test/1",
        attribute: "DevEnum",
        dataType: "DevEnum",
        dataFormat: "scalar",
        isNumeric: true,
        unit: "",
        enumlabels: ["OFF", "ON", "FAULT"],
        write: () => {},
        value: 1,
        writeValue: 1,
        label: "",
        history: [],
        timestamp: 0,
        quality: "VALID",
      },
      size: 1,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    renderComponent("run", inputs);

    const input = screen.getByPlaceholderText("1") as HTMLInputElement;

    // test send default without change event
    await waitFor(() => {
      expect(input.value).toBe("0");
    });

    fireEvent.submit(input);

    await waitFor(() => {
      expect(input.value).toBe("0");
    });
 
    fireEvent.change(input, { target: { value: "FAULT" } });
    fireEvent.submit(input);
 
    await waitFor(() => {
      expect(input.value).toBe("FAULT");
    });
  })

  it("submit DevBoolean with f", async () => {
    const inputs = {
      showDevice: false,
      showTangoDB: false,
      showAttribute: "Label",
      alignValueRight: true,
      attribute: {
        device: "tangodb://sys/tg_test/1",
        attribute: "DevBoolean",
        dataType: "DevBoolean",
        dataFormat: "scalar",
        isNumeric: true,
        unit: "",
        enumlabels: [],
        write: () => {},
        value: 1,
        writeValue: 1,
        label: "",
        history: [],
        timestamp: 0,
        quality: "VALID",
      },
      size: 1,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };

    renderComponent("run", inputs);
    const input = screen.getByPlaceholderText("1") as HTMLInputElement;
    fireEvent.change(input, { target: { value: "f" } });
    fireEvent.submit(input);
    expect(input.value).toBe("f");
  });

  it("submit DevDouble not accepted input", async () => {
    const inputs = {
      showDevice: false,
      showTangoDB: false,
      attribute: {
        device: "tangodb://sys/tg_test/1",
        attribute: "DevDouble",
        dataType: "DevDouble",
        dataFormat: "scalar",
        isNumeric: true,
        unit: "",
        enumlabels: [],
        write: () => {},
        value: 1,
        writeValue: 1,
        label: "",
        history: [],
        timestamp: 0,
        quality: "VALID",
      },
      size: 1,
      showAttribute: "Label",
      alignValueRight: true,
      textColor: "red",
      backgroundColor: "blue",
      font: "mySelect",
      widgetCss: "",
      title: "",
    };
    renderComponent("run", inputs);

    const input = screen.getByPlaceholderText("1.00") as HTMLInputElement;
    fireEvent.change(input, { target: { value: "1as" } });
    fireEvent.submit(input);
    expect(input.value).toBe("1as");
  });
});
