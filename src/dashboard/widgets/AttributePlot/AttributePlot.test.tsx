import React from "react";
import { useStore as useStoreMock } from "react-redux";
import { act, render, renderHook } from "@testing-library/react";
import { configure } from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";

import { TypedInputs } from "../types";
import AttributePlot, {
  Inputs,
  TimeSeriesData,
  getAttributeFullName,
  getTrace,
  useThrottledData,
} from "./AttributePlot";
import PlotMock from "./Plot";

// Mock useStore
jest.mock("react-redux", () => ({
  ...jest.requireActual("react-redux"),
  useStore: jest.fn(),
}));
const useStore = useStoreMock as jest.Mock;

// Mock Plot component
jest.mock("./Plot", () => {
  return {
    __esModule: true,
    ...jest.requireActual("./Plot"),
    default: jest.fn(),
  };
});
const Plot = PlotMock as jest.Mock;

const writeArray: any = [];
const timestamp = Date.now();

const mockedState = {
  messages: {
    "sys/tg_test/1": {
      attributes: {
        long_scalar: {
          values: [228, 228, 91],
          quality: ["ATTR_VALID", "ATTR_VALID", "ATTR_VALID"],
          timestamp: [1690542017.171402, 1690542017.127803, 1690542041.218554],
        },
        short_scalar: {
          values: [170, 170, 90],
          quality: ["ATTR_VALID", "ATTR_VALID", "ATTR_VALID"],
          timestamp: [1690542017.181211, 1690542017.128467, 1690542041.218627],
        },
      },
    },
  },
  ui: {
    mode: "run",
  },
};

configure({ adapter: new Adapter() });

describe("AttributePlot", () => {
  beforeEach(() => {
    // Return a mocked state
    useStore.mockImplementation(() => ({
      getState: () => mockedState,
    }));

    // Default the original implementation for the Plot component
    Plot.mockImplementation(jest.requireActual("./Plot").default);
  });

  const complexInput = [
    {
      attribute: {
        device: "sys/tg_test/1",
        attribute: "short_scalar",
        label: "ShortScalar",
        history: [],
        dataType: "",
        dataFormat: "",
        isNumeric: true,
        unit: "",
        enumlabels: [],
        write: writeArray,
        value: [0, 1, 2, 3, 4],
        writeValue: "",
        timestamp: timestamp,
        quality: "VALID",
      },
      lineColor: "green",
      showAttribute: "Label",
      yAxis: "left",
      yAxisDisplay: "Label",
    },
  ];

  let myInput: TypedInputs<Inputs> = {
    timeWindow: 120,
    showZeroLine: true,
    yLogarithmicScale: false,
    xLogarithmicScale: false,
    xScientificNotation: false,
    yScientificNotation: false,
    textColor: "",
    showTangoDB: false,
    backgroundColor: "cyan",
    attributes: complexInput,
  };

  it("render widget with normal scenario edit mode", async () => {
    const { container } = render(
      <AttributePlot
        mode={"edit"}
        t0={1}
        actualWidth={100}
        actualHeight={100}
        inputs={myInput}
        id={1}
      />
    );

    expect(container.innerHTML).toContain("width: 100%");
    expect(container.innerHTML).toContain("height: 100%");
  });

  it("render widget with normal scenario library mode", async () => {
    const { container } = render(
      <AttributePlot
        mode={"library"}
        t0={1}
        actualWidth={100}
        actualHeight={100}
        inputs={myInput}
        id={1}
      />
    );
    expect(container.innerHTML).toContain("width: 100%");
    expect(container.innerHTML).toContain("height: 100%");
  });

  it("render widget with normal scenario run mode", async () => {
    const { container } = render(
      <AttributePlot
        mode={"run"}
        t0={1}
        actualWidth={100}
        actualHeight={100}
        inputs={myInput}
        id={1}
      />
    );
    expect(container.innerHTML).toContain("width: 100%");
    expect(container.innerHTML).toContain("height: 100%");
  });

  it("passes correct props to Plot component", async () => {
    const t0 = 1;

    render(
      <AttributePlot
        mode={"run"}
        t0={t0}
        actualWidth={100}
        actualHeight={100}
        inputs={myInput}
        id={1}
      />
    );

    const trace = {
      axisLocation: "left",
      fullName: "sys/tg_test/1/short_scalar",
      lineColor: "green",
    };
    const attribute =
      mockedState.messages["sys/tg_test/1"].attributes["short_scalar"];

    // Check that the Plot component is called twice due to the useThrottledData hook:
    expect(Plot).toHaveBeenCalledTimes(2);
    // 1. with empty data first for initialization of the hook
    expect(Plot).toHaveBeenNthCalledWith(
      1,
      expect.objectContaining({
        traces: [{ ...trace, x: [], y: [] }],
      }),
      {}
    );
    // 2. with actual data when the background task has been initialized
    expect(Plot).toHaveBeenNthCalledWith(
      2,
      expect.objectContaining({
        traces: [
          {
            ...trace,
            x: attribute.timestamp.map((t) => t - t0),
            y: attribute.values,
          },
        ],
      }),
      {}
    );
  });
});

/**
 * This just flattens the state messages to TimeSeriesData
 * {[fullName: string]: {timestamp: number[], values: number[]}.
 * @param {any} messages - The first number.
 * @returns {TimeSeriesData} flattened result.
 */
const messagesToTimeSeriesData = (messages: any): TimeSeriesData =>
  Object.keys(messages).reduce((acc, device) => {
    const attributes = messages[device].attributes;

    Object.keys(attributes).forEach((attr) => {
      const fullName = `${device}/${attr}`;
      acc[fullName] = {
        timestamp: attributes[attr].timestamp,
        values: attributes[attr].values,
      };
    }, acc);

    return acc;
  }, {});

const complexInput = [
  {
    attribute: {
      device: "sys/tg_test/1",
      attribute: "long_scalar",
      label: "long_scalar",
      history: [],
      dataType: "DevLong",
      enumlabels: [],
      dataFormat: "scalar",
      isNumeric: true,
      unit: "",
      write: writeArray,
      value: [0, 1, 2, 3, 4],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID",
    },
    showAttribute: "Label",
    yAxis: "left",
    lineColor: "#935c5c",
  },
  {
    attribute: {
      device: "sys/tg_test/1",
      attribute: "short_scalar",
      label: "short_scalar",
      history: [],
      dataType: "DevShort",
      enumlabels: [],
      dataFormat: "scalar",
      isNumeric: true,
      unit: "",
      write: writeArray,
      value: [0, 1, 2, 3, 4],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID",
    },
    showAttribute: "Label",
    yAxis: "left",
    lineColor: "#20b15f",
  },
];

describe("getTrace", () => {
  // Flatten the state messages and convert to TimeSeriesData
  // (this is generated by the useThrottledData hook)
  const timeSeriesData = messagesToTimeSeriesData(mockedState.messages);

  it("returns a trace with valid state messages", () => {
    complexInput.forEach((attribute) => {
      const fullName = getAttributeFullName(attribute.attribute);
      const trace = getTrace({ attribute, data: timeSeriesData });
      expect(trace).toEqual({
        fullName,
        axisLocation: attribute.yAxis,
        lineColor: attribute.lineColor,
        x: timeSeriesData[fullName].timestamp,
        y: timeSeriesData[fullName].values,
      });
    });
  });

  it("returns an empty value with invalid state messages", () => {
    complexInput.forEach((attribute) => {
      const fullName = getAttributeFullName(attribute.attribute);
      const trace = getTrace({ attribute, data: {} });
      expect(trace).toEqual({
        fullName,
        axisLocation: attribute.yAxis,
        lineColor: attribute.lineColor,
        x: [],
        y: [],
      });
    });
  });
});

const updateState = (state, attributes: any = {}) => {
  const update = (old, new_) => {
    return new_
      ? {
          ...old,
          values: [...old.values, ...(new_.values || [])],
          timestamp: [...old.timestamp, ...(new_.timestamp || [])],
        }
      : old;
  };
  const message = state.messages["sys/tg_test/1"];

  return {
    ...state,
    messages: {
      ...state.messages,
      "sys/tg_test/1": {
        ...message,
        attributes: {
          ...message.attributes,
          long_scalar: update(
            message.attributes.long_scalar,
            attributes.long_scalar
          ),
          short_scalar: update(
            message.attributes.short_scalar,
            attributes.short_scalar
          ),
        },
      },
    },
  };
};

describe("useThrottledData", () => {
  const mockedGetState = jest.fn();

  beforeEach(() => {
    jest.useFakeTimers();
    jest.resetAllMocks();

    mockedGetState.mockImplementation(() => mockedState);

    useStore.mockImplementation(() => ({
      getState: mockedGetState,
    }));
  });

  it("returns initial time series data when active", () => {
    const { result } = renderHook(() =>
      useThrottledData({
        attributes: complexInput.map((attr) => attr.attribute),
        active: true,
      })
    );

    expect(result.current).toEqual(
      messagesToTimeSeriesData(mockedState.messages)
    );
  });

  it("return an empty object when not active", () => {
    const { result } = renderHook(() =>
      useThrottledData({
        attributes: complexInput.map((attr) => attr.attribute),
        active: false,
      })
    );

    expect(result.current).toEqual({});
  });

  it("return an empty object when attribute is invalid", () => {
    const invalid: any = {};
    const { result } = renderHook(() =>
      useThrottledData({
        attributes: [invalid],
        active: true,
      })
    );

    expect(result.current).toEqual({});
  });

  it("updates the value only on interval", () => {
    const { result } = renderHook(() =>
      useThrottledData({
        attributes: complexInput.map((attr) => attr.attribute),
        active: true,
        delay: 500,
      })
    );
    const initialData = result.current;
    mockedGetState.mockClear();

    // Update the messages (first) and fast-forward 100 ms.
    // We don't expect that the data is updated.
    const firstUpdate = updateState(mockedState, {
      long_scalar: { timestamp: [1690542042.0], values: [2] },
    });
    mockedGetState.mockImplementation(() => firstUpdate);
    act(() => {
      jest.advanceTimersByTime(100);
    });
    expect(mockedGetState).not.toHaveBeenCalled();
    expect(result.current).toEqual(initialData);

    // Update the messages (second) and fast-forward 100 ms.
    // We don't expect that the data is updated.
    const secondUpdate = updateState(firstUpdate, {
      long_scalar: { timestamp: [1690542043.0], values: [3] },
    });
    mockedGetState.mockImplementation(() => secondUpdate);
    act(() => {
      jest.advanceTimersByTime(100);
    });
    expect(mockedGetState).not.toHaveBeenCalled();
    expect(result.current).toEqual(initialData);

    // Update the messages (third) and fast-forward 400 ms.
    // We expect that the data is updated and contains values from the other updates.
    const thirdUpdate = updateState(secondUpdate, {
      long_scalar: { timestamp: [1690542044.0], values: [4] },
    });
    mockedGetState.mockImplementation(() => thirdUpdate);
    act(() => {
      jest.advanceTimersByTime(400);
    });
    expect(mockedGetState).toHaveBeenCalled();
    expect(result.current).toEqual(
      messagesToTimeSeriesData(thirdUpdate.messages)
    );
  });

  it("doesn't trigger rerenders when the state has not been changed", () => {
    const { result } = renderHook(() =>
      useThrottledData({
        attributes: complexInput.map((attr) => attr.attribute),
        active: true,
        delay: 500,
      })
    );
    const initialData = result.current;
    mockedGetState.mockClear();

    act(() => {
      jest.advanceTimersByTime(600);
    });
    expect(mockedGetState).toHaveBeenCalled();
    expect(result.current).toBe(initialData);
  })
});
