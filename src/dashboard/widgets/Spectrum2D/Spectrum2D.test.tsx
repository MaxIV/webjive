import React from "react";
import { configure, shallow } from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import Spectrum2D, { Inputs } from "./Spectrum2D";
import { TypedInputs } from "../types";
import Spectrum2DValues from "./Spectrum2DValues";

configure({ adapter: new Adapter() });

jest.mock("./Spectrum2DValues", () => () => <div>Mock Spectrum2DValues</div>);

describe("Spectrum2D Tests", () => {
  let myInput: TypedInputs<Inputs>;
  var writeArray: any = [];
  let myAttributeInputX: any = {};
  let myAttributeInputY: any = [];
  var date = new Date();
  var timestamp = date.getTime();

  beforeEach(() => {
    myAttributeInputX = {
      device: "sys/tg_test/1",
      attribute: "time_range",
      label: "TimeRange",
      history: [],
      dataType: "",
      dataFormat: "",
      isNumeric: true,
      unit: "",
      enumlabels: [],
      write: writeArray,
      value: [1, 2, 3, 4, 5],
      writeValue: "",
      timestamp: timestamp,
      quality: "VALID",
    };
    myAttributeInputY = [
      {
        attribute: {
          device: "sys/tg_test/1",
          attribute: "voltage",
          label: "Voltage",
          history: [],
          dataType: "",
          dataFormat: "",
          isNumeric: true,
          unit: "",
          enumlabels: [],
          write: writeArray,
          value: [0, 1, 2, 3, 4],
          writeValue: "",
          timestamp: timestamp,
          quality: "VALID",
        },
      },
    ];
  });

  it("renders library mode true without crashing with logarithmic", () => {
    myInput = {
      attributeX: myAttributeInputX,
      attributes: myAttributeInputY,
      showTitle: true,
      showAttribute: "Name",
      showTangoDB: false,
      xScientificNotation: false,
      yScientificNotation: false,
      xLogarithmicScale: true,
      yLogarithmicScale: true,
      plotStyling: "bar",
      textColor: "",
      backgroundColor: "",
    };

    const element = React.createElement(Spectrum2D.component, {
      mode: "library",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 1,
    });
    expect(shallow(element).html()).toContain("height:150px");
  });

  it("renders library mode true without crashing with exponent", () => {
    myInput = {
      attributeX: myAttributeInputX,
      attributes: myAttributeInputY,
      showTitle: true,
      showAttribute: "Name",
      showTangoDB: false,
      xScientificNotation: true,
      yScientificNotation: true,
      xLogarithmicScale: false,
      yLogarithmicScale: false,
      plotStyling: "markers",
      textColor: "",
      backgroundColor: "",
    };

    const element = React.createElement(Spectrum2D.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 2,
    });
    expect(shallow(element).html()).not.toContain("height:150px");
  });

  it("renders edit mode true without crashing with exponent", () => {
    myInput = {
      attributeX: myAttributeInputX,
      attributes: myAttributeInputY,
      showTitle: true,
      showAttribute: "Label",
      showTangoDB: false,
      xScientificNotation: true,
      yScientificNotation: true,
      xLogarithmicScale: false,
      yLogarithmicScale: false,
      plotStyling: "markers",
      textColor: "",
      backgroundColor: "",
    };

    const element = React.createElement(Spectrum2D.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
      id: 2,
    });
    expect(shallow(element).html()).not.toContain("height:150px");
  });

  it("transforms y-axis values to logarithmic scale when logarithmic scale is true", () => {
    myInput = {
      attributeX: myAttributeInputX,
      attributes: myAttributeInputY,
      showTitle: true,
      showAttribute: "Name",
      showTangoDB: false,
      xScientificNotation: false,
      yScientificNotation: false,
      xLogarithmicScale: false,
      yLogarithmicScale: true,
      plotStyling: "bar",
      textColor: "",
      backgroundColor: "",
    };

    const element = shallow(
      React.createElement(Spectrum2D.component, {
        mode: "run",
        t0: 1,
        actualWidth: 100,
        actualHeight: 100,
        inputs: myInput,
        id: 1,
      })
    );

    const props: any = element.find(Spectrum2DValues).props();
    expect(props.layout.yaxis.type).toEqual("log");
  });

  it("transforms x-axis values to logarithmic scale when logarithmic scale is true", () => {
    myInput = {
      attributeX: myAttributeInputX,
      attributes: myAttributeInputY,
      showTitle: true,
      showAttribute: "Name",
      showTangoDB: false,
      xScientificNotation: false,
      yScientificNotation: false,
      xLogarithmicScale: true,
      yLogarithmicScale: false,
      plotStyling: "bar",
      textColor: "",
      backgroundColor: "",
    };

    const element = shallow(
      React.createElement(Spectrum2D.component, {
        mode: "run",
        t0: 1,
        actualWidth: 100,
        actualHeight: 100,
        inputs: myInput,
        id: 1,
      })
    );

    const props: any = element.find(Spectrum2DValues).props();
    expect(props.layout.xaxis.type).toEqual("log");
  });

  it("transforms y-axis values to scientific notation when scientific notation is true", () => {
    myInput = {
      attributeX: myAttributeInputX,
      attributes: myAttributeInputY,
      showTitle: true,
      showAttribute: "Name",
      showTangoDB: false,
      xScientificNotation: false,
      yScientificNotation: true,
      xLogarithmicScale: false,
      yLogarithmicScale: false,
      plotStyling: "bar",
      textColor: "",
      backgroundColor: "",
    };

    const element = shallow(
      React.createElement(Spectrum2D.component, {
        mode: "run",
        t0: 1,
        actualWidth: 100,
        actualHeight: 100,
        inputs: myInput,
        id: 1,
      })
    );

    const props: any = element.find(Spectrum2DValues).props();
    expect(props.layout.yaxis.exponentformat).toEqual("e");
  });

  it("transforms x-axis values to scientific notation when scientific notation is true", () => {
    myInput = {
      attributeX: myAttributeInputX,
      attributes: myAttributeInputY,
      showTitle: true,
      showAttribute: "Name",
      showTangoDB: false,
      xScientificNotation: true,
      yScientificNotation: false,
      xLogarithmicScale: false,
      yLogarithmicScale: false,
      plotStyling: "bar",
      textColor: "",
      backgroundColor: "",
    };

    const element = shallow(
      React.createElement(Spectrum2D.component, {
        mode: "run",
        t0: 1,
        actualWidth: 100,
        actualHeight: 100,
        inputs: myInput,
        id: 1,
      })
    );

    const props: any = element.find(Spectrum2DValues).props();
    expect(props.layout.xaxis.exponentformat).toEqual("e");
  });
});
