import React, { useCallback, useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import TangoAPI from "../../../../shared/api/tangoAPI";
import {
  BooleanInputDefinition,
  DeviceInputDefinition,
  StringInputDefinition,
  StyleInputDefinition,
  WidgetDefinition,
} from "../../../types";
import {
  SET_INPUT,
  SELECT_WIDGETS,
} from "../../../../shared/state/actions/actionTypes";
import { IndexPath } from "../../../types";

import "./MacroButton.css";
import { StateIndicatorLabel } from "../../../../shared/ui/components/StateIndicatorLabel";
import { WidgetProps } from "../../types";
import { DeviceConsumer } from "../../../components/DevicesProvider";
import { Button, Container, Row, Col } from "react-bootstrap";
import { Tooltip } from "react-tooltip";
import "react-tooltip/dist/react-tooltip.css";
import cx from "classnames";
import {
  getAttributeLastTimeStampFromState,
  getAttributeLastValueFromState,
} from "../../../../shared/utils/getLastValueHelper";
import { IRootState } from "../../../../shared/state/reducers/rootReducer";
import { parseCss } from "../../../components/Inspector/StyleSelector";
import { splitFullPath } from "../../../DBHelper";

// Types

type Inputs = {
  door: DeviceInputDefinition;
  macroserver: DeviceInputDefinition;
  pool: DeviceInputDefinition;
  outerDivCss: StyleInputDefinition;
  savedMacro?: StringInputDefinition;
  savedMacroArg?: StringInputDefinition;
  alignButtonRight: BooleanInputDefinition;
};

interface MacroArg {
  name: string;
  value: string;
  description: string;
  type: string; //Moveable, Float etc.
}

interface Macro {
  name: string;
  description: string;
}

interface TangoExecutionResult {
  ok: boolean;
  output: string[];
  message: string;
}

type Props = WidgetProps<Inputs>;

const _ARG_SEPARATOR = ", ";

// Utils

// Concatinate all macro arguments into a single string
const concatinateMacroArgs = (macroArgs: MacroArg[]) => {
  const macroArgString = macroArgs
    .filter((arg: MacroArg) => {
      return arg.value.length;
    })
    .map((arg: MacroArg) => {
      return arg.value;
    })
    .join(_ARG_SEPARATOR)
    .trim();

  return macroArgString;
};

const getArgumentsForAction = (
  action: string,
  currentMacroName: string,
  macroArg: string
) => {
  if (action === "RunMacro") {
    return [currentMacroName, ...macroArg.split(_ARG_SEPARATOR)];
  } else if (action === "GetMacroInfo") {
    return [currentMacroName];
  } else {
    return null;
  }
};

// Custom Hooks

const useMacroArgs = (
  mode: string
): [MacroArg[], React.Dispatch<React.SetStateAction<MacroArg[]>>] => {
  const [selectedMacroArgs, setSelectedMacroArgs] = useState<MacroArg[]>([]);

  useEffect(() => {
    if (mode === "edit") {
      setSelectedMacroArgs([
        {
          name: "testMacro",
          value: "macro",
          description: "Taranta test macro",
          type: "Movable",
        },
      ]);
    } else {
      setSelectedMacroArgs([]);
    }
  }, [mode]);

  return [selectedMacroArgs, setSelectedMacroArgs];
};

const useStateValue = ({ doorName, timestamp }) => {
  return useSelector((state: IRootState) => {
    const latestStateValue = getAttributeLastValueFromState(
      state.messages,
      doorName,
      "state"
    );
    if (latestStateValue === "RUNNING" && timestamp.current === undefined) {
      const latestTimestamp = getAttributeLastTimeStampFromState(
        state.messages,
        doorName,
        "output"
      );
      timestamp.current = latestTimestamp === null ? 0 : latestTimestamp;
    }
    return latestStateValue;
  });
};

const useMotorList = (poolName: string): string[] => {
  return useSelector((state: IRootState) => {
    return (
      getAttributeLastValueFromState(state.messages, poolName, "motorlist") ||
      []
    );
  });
};

const useMacroList = (macroServerName: string): string[] => {
  return useSelector((state: IRootState) => {
    return (
      getAttributeLastValueFromState(
        state.messages,
        macroServerName,
        "macrolist"
      ) || []
    );
  });
};

const useOutputValues = (doorName: string) => {
  return useSelector((state: IRootState) => {
    if (state.messages?.[doorName]) {
      const output = state.messages[doorName].attributes.output;
      return output.timestamp.map((timestamp, index) => ({
        timestamp,
        value: output.values[index],
      }));
    }
    return [];
  });
};

// Component

const MacroButton: React.FC<Props> = (props) => {
  const {
    door,
    alignButtonRight,
    macroserver,
    outerDivCss,
    pool,
    savedMacro,
    savedMacroArg,
  } = props.inputs;

  const dispatch = useDispatch();
  const timestamp = useRef<number | undefined>(undefined);
  const outputMessages = useRef<string[]>([]);
  const [macroArg, setMacroArg] = useState<string>("");
  const [selectedMacro, setSelectedMacro] = useState<Macro>({
    description: "",
    name: "",
  });

  const macroList = useMacroList(macroserver.name);
  const motorList = useMotorList(pool.name);
  const outputValues = useOutputValues(door.name);

  const stateValue = useStateValue({
    doorName: door.name,
    timestamp: timestamp,
  });

  const [selectedMacroArgs, setSelectedMacroArgs] = useMacroArgs(props.mode);

  // Save macro name or args when the widget is running
  const saveSettingsWhileRunning = useCallback(
    (path: IndexPath, value: string) => {
      // Select the current macro button widget
      dispatch({
        type: SELECT_WIDGETS,
        ids: [props.id],
      });
      // Update the selected widget with the selected macro or macro args
      dispatch({
        type: SET_INPUT,
        path: path,
        value: value,
        widgetType: "MACROBUTTON",
      });
    },
    [dispatch, props.id]
  );

  // Watch for changes to `selectedMacro` and save macro name to the widget
  useEffect(() => {
    if (selectedMacro.name.length === 0) return;

    saveSettingsWhileRunning(["savedMacro"], selectedMacro.name);

    // Clear macro args when a new macro is selected
    saveSettingsWhileRunning(["savedMacroArg"], "");
  }, [selectedMacro, saveSettingsWhileRunning]);

  // Watch for changes to `macroArg`, save string to the hidden input values
  useEffect(() => {
    if (macroArg.length === 0) return;

    saveSettingsWhileRunning(["savedMacroArg"], macroArg);
  }, [macroArg, saveSettingsWhileRunning]);

  // Get filtered outputs
  useEffect(() => {
    let newOutput: any[] = [];
    if (outputValues.length && timestamp.current !== undefined) {
      newOutput = [...outputValues]
        .filter((output) => output.timestamp > timestamp.current!)
        .map((output) => output.value);
    }
    outputMessages.current = newOutput;
  }, [outputValues]);

  const onChangeMacroArgValue = (event: any) => {
    const { value, name } = event.target;
    const macroArgs = selectedMacroArgs;
    macroArgs[macroArgs.findIndex((obj) => obj.name === name)].value = value;

    setSelectedMacroArgs(macroArgs);
    setMacroArg(concatinateMacroArgs(macroArgs));
    outputMessages.current = [];
  };

  const setMacro = (event: any, tangoDB: string) => {
    const selectedMacro = {
      description: "",
      name: event.target.value,
    };
    setSelectedMacro(selectedMacro);
    executeCommand("macroserver", "GetMacroInfo", selectedMacro.name);
    outputMessages.current = [];

    // Reset the macro args when a new macro is selected
    setMacroArg("");
  };

  const handleMacro = (tangoDB: string, device: string, action: string) => {
    // Reset timestamp to clear output if we are starting the macro
    if (action === "RunMacro") {
      timestamp.current = undefined;
    }

    executeCommand(device, action);
  };

  const getOutputError = () => {
    const executeAsync = async () => {
      const [tangoDB, doorDev] = splitFullPath(door.name);

      if (door) {
        const error = await TangoAPI.fetchAttributesValues(tangoDB, [
          doorDev + "/Error",
        ]);
        return error[0].value ? error[0].value[0] : null;
      } else {
        alert("No door device");
      }
    };

    return executeAsync();
  };

  async function executeCommand(
    device: string,
    action: string,
    selectedMacroName = ""
  ) {
    let currentMacroName = selectedMacroName;
    if (currentMacroName === "" && selectedMacro.name) {
      currentMacroName = selectedMacro.name;
    } else if (currentMacroName === "" && savedMacro) {
      currentMacroName = savedMacro;
    }

    let currentMacroArg = (macroArg || savedMacroArg) ?? "";
    const argument = getArgumentsForAction(
      action,
      currentMacroName,
      currentMacroArg
    );

    const deviceName = device === "door" ? door.name : macroserver.name;
    const selectedMacroArgs: MacroArg[] = [];
    const [tangoDBCurrent, devName] = splitFullPath(deviceName);

    const executionResult: TangoExecutionResult | null = await TangoAPI.executeCommand(
      tangoDBCurrent,
      devName,
      action,
      argument
    );
    let alertText: string | null = null;

    if (device === "macroserver" && executionResult) {
      let parameters = JSON.parse(executionResult.output[0]);

      for (let i = 0; i < parameters.parameters.length; i++) {
        let macro_arg_item: MacroArg = {
          description: parameters.parameters[i].description,
          name: parameters.parameters[i].name,
          value: "",
          type: parameters.parameters[i].type,
        };
        selectedMacroArgs.push(macro_arg_item);
      }
      setSelectedMacroArgs(selectedMacroArgs);
      setSelectedMacro({
        description: parameters.description.replace(/\s\s+/g, " "),
        name: currentMacroName,
      });
    } else {
      // output error is only for door device
      const error = await getOutputError();
      if (error) {
        alertText = error;
      }
    }

    if (!executionResult || !executionResult.ok) {
      alertText = `There was a problem executing ${action}`;
    }
    alertText && alert(alertText);
  }

  const { mode } = props;
  let outerDivCssObj = parseCss(outerDivCss).data;
  let name = "";
  let alias = "";

  if (door) {
    name = door.name;
    alias = door.alias;
  }

  return (
    <div className="macro-button" style={outerDivCssObj}>
      <div className="door-row">
        <Container>
          <Row>
            <Col xs={5}>
              <StateIndicatorLabel
                state={mode === "run" ? stateValue : "UNKNOWN"}
              />
              <span className="device-alias">{alias || name || "Device"}</span>
            </Col>
            <Col xs={1}>
              <Tooltip
                id="my-tooltip"
                content={`(${selectedMacro.name}) ${selectedMacro.description}`}
                style={{
                  marginLeft: "1.2rem",
                  width: "auto",
                  display: selectedMacro.description === "" ? "none" : "block",
                  direction: "ltr",
                }}
              >
                <i className={cx("DescriptionDisplay fa fa-info-circle")} />
              </Tooltip>
            </Col>
          </Row>
        </Container>
        <Container>
          <Row>
            <Col xs={6}>
              <select
                className="form-control macro-list-dropdown full-width"
                disabled={["library", "edit"].includes(props.mode)}
                style={{
                  display: ["library", "edit"].includes(props.mode)
                    ? "block"
                    : "none",
                }}
              >
                <option value="" disabled>
                  {macroList === undefined
                    ? "No Macros available"
                    : "Select Macro"}
                </option>
              </select>
            </Col>

            <Col xs={6}>
              <input
                type="text"
                className="form-control full-width"
                style={{
                  borderTopLeftRadius: 4,
                  borderBottomLeftRadius: 4,
                  display: ["library", "edit"].includes(props.mode)
                    ? "block"
                    : "none",
                }}
                placeholder={"Macro Args (separated by whitespace)"}
              />
            </Col>
          </Row>
          <Row style={{ marginTop: "0.5rem" }}>
            <Col xs={6} className={alignButtonRight ? "ml-auto" : ""}>
              <Button
                className="full-width form-control"
                style={{
                  width: "100%",
                  display: ["library", "edit"].includes(props.mode)
                    ? "block"
                    : "none",
                }}
                size="lg"
                variant="success"
              >
                Run
              </Button>
            </Col>
          </Row>
        </Container>
        <div className="door-row">
          <div className="input-group">
            {mode === "run" && (
              <>
                <DeviceConsumer>
                  {({ tangoDB }) => (
                    <>
                      <Container>
                        <Row>
                          <Col xs={6}>
                            <select
                              id="macroListSelect"
                              className="form-control macro-list-dropdown"
                              value={selectedMacro.name || savedMacro}
                              disabled={"library" === props.mode}
                              onChange={(e) => setMacro(e, tangoDB)}
                            >
                              <option value="" disabled>
                                {macroList === undefined
                                  ? "No Macros available"
                                  : "Select Macro"}
                              </option>
                              {macroList.map(
                                (macroItem: string, index: number) => {
                                  return (
                                    <option key={index} value={macroItem}>
                                      {macroItem}
                                    </option>
                                  );
                                }
                              )}
                            </select>
                          </Col>
                          <Col xs={6}>
                            <input
                              type="text"
                              className="form-control"
                              style={{
                                borderTopLeftRadius: 4,
                                borderBottomLeftRadius: 4,
                              }}
                              value={macroArg || savedMacroArg}
                              readOnly={true}
                              placeholder={"Macro Args string will appear here"}
                              data-ui-id="macro-args"
                            />
                          </Col>
                        </Row>
                      </Container>

                      <Container>
                        <>
                          <Row
                            style={{
                              display: `${
                                selectedMacroArgs.length === 0 ? "none" : "flex"
                              }`,
                            }}
                          >
                            {selectedMacroArgs.map(
                              (arg: MacroArg, index: number) => {
                                return arg.type === "Moveable" ? (
                                  <Col sm={4} key={index}>
                                    <select
                                      id="movableMacroSelect"
                                      key={index}
                                      className="form-control"
                                      name={arg.name}
                                      value={arg.value}
                                      onChange={onChangeMacroArgValue}
                                      style={{ marginTop: "0.5rem" }}
                                    >
                                      <option value="" disabled>
                                        {motorList.length === 0
                                          ? "No Motors available"
                                          : "Select Motor"}
                                      </option>
                                      {motorList.map(
                                        (motor: string, index: number) => {
                                          const motorName = JSON.parse(motor)
                                            .name;
                                          return (
                                            <option
                                              key={index}
                                              value={motorName}
                                            >
                                              {motorName}
                                            </option>
                                          );
                                        }
                                      )}
                                    </select>
                                  </Col>
                                ) : (
                                  <Col sm={4} key={index}>
                                    <input
                                      key={index}
                                      type="text"
                                      className="form-control"
                                      style={{
                                        borderTopLeftRadius: 4,
                                        borderBottomLeftRadius: 4,
                                        marginTop: "0.5rem",
                                      }}
                                      placeholder={arg.name}
                                      onChange={onChangeMacroArgValue}
                                      name={arg.name}
                                      value={arg.value}
                                    />
                                  </Col>
                                );
                              }
                            )}
                          </Row>
                          <Row style={{ marginTop: "0.5rem" }}>
                            <Col
                              sm={4}
                              className={alignButtonRight ? "ml-auto" : ""}
                            >
                              <Button
                                id="executeButton"
                                className="full-width form-control"
                                onClick={() =>
                                  handleMacro(
                                    tangoDB,
                                    "door",
                                    stateValue === "RUNNING"
                                      ? "StopMacro"
                                      : "RunMacro"
                                  )
                                }
                                style={{ width: "100%" }}
                                size="lg"
                                variant={
                                  stateValue === "RUNNING"
                                    ? "danger"
                                    : "success"
                                }
                              >
                                {stateValue === "RUNNING" ? "Stop" : "Run"}
                              </Button>
                            </Col>
                          </Row>
                        </>
                      </Container>
                    </>
                  )}
                </DeviceConsumer>
                <br />
              </>
            )}
          </div>
          <div className="result-section">
            <table className="result-table">
              <tbody>
                {outputMessages.current.length === 0 ? (
                  // No output messages available
                  <tr>
                    <td style={{ color: "grey", marginBottom: "0" }}>
                      Door output will be displayed here
                    </td>
                  </tr>
                ) : (
                  // Iterate over the output messages and display them
                  outputMessages.current.map(
                    (singleStringRow: string, index: number) => {
                      return (
                        <tr key={index}>
                          <td>
                            <pre>{singleStringRow}</pre>
                          </td>
                        </tr>
                      );
                    }
                  )
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

const definition: WidgetDefinition<Inputs> = {
  type: "MACROBUTTON",
  name: "Macro Button",
  defaultHeight: 12,
  defaultWidth: 32,
  inputs: {
    door: {
      type: "device",
      label: "Door",
      publish: "$door",
    },
    macroserver: {
      type: "device",
      label: "MacroServer",
      publish: "$macroserver",
    },
    pool: {
      type: "device",
      label: "Pool",
      publish: "$pool",
    },
    alignButtonRight: {
      type: "boolean",
      label: "Align the Run/Stop Button to Right",
      default: false,
    },
    savedMacro: {
      type: "string",
      renderAs: "hidden",
    },
    savedMacroArg: {
      type: "string",
      renderAs: "hidden",
    },
    outerDivCss: {
      type: "style",
      label: "Outer Div CSS",
    },
  },
};

const MacroButtonExport = { definition, component: MacroButton };
export default MacroButtonExport;
