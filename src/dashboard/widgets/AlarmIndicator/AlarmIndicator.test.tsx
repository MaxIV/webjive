import React from "react";
import { AttributeInput } from "../../types";
import "../../../shared/tests/globalMocks";
import { configure, mount } from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import AlarmIndicator from "./AlarmIndicator";
import { Provider } from "react-redux";
import configureStore from "../../../shared/state/store/configureStore";

jest.mock("./AlarmIndicatorValues", () => () => <div>Mock AlarmIndicatorValues</div>);

configure({ adapter: new Adapter() });
const store = configureStore();

interface Input {
  attribute: AttributeInput;
  textColor: string;
  backgroundColor: string;
  size: number;
  font: string;
  widgetCss: string;
}

describe("AttributeDisplayTests", () => {
  let myAttributeInput: AttributeInput;
  let myInput: Input;
  var writeArray: any = [];
  var date = new Date();
  var timestamp = date.getTime();

  it("renders all false without crashing", () => {
    myAttributeInput = {
        device: "alarm/taranta/01",
        attribute: "taranta_faultalarm",
        label: "Taranta Fault Alarm",
        history: [],
        dataType: "string",
        dataFormat: "alarm",
        isNumeric: false,
        unit: "",
        enumlabels: ["OFF", "ON"],
        write: writeArray,
        value: true,
        writeValue: "",
        quality: "valid",
        timestamp: timestamp,
    };

    myInput = {
      attribute: myAttributeInput,
      textColor: "",
      backgroundColor: "",
      size: 1,
      font: "",
      widgetCss: "",
    };
    let element = React.createElement(AlarmIndicator.component, {
      id: 1,
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    let wrapper = mount(<Provider store={store}>{element}</Provider>);
    expect(wrapper.find("#AlarmIndicator").exists()).toBe(true);
  });

  it("renders in library mode without crashing", () => {
    myAttributeInput = {
        device: "alarm/taranta/01",
        attribute: "taranta_faultalarm",
        label: "Taranta Fault Alarm",
        history: [],
        dataType: "string",
        dataFormat: "alarm",
        isNumeric: false,
        unit: "",
        enumlabels: ["OFF", "ON"],
        write: writeArray,
        value: true,
        writeValue: "",
        quality: "valid",
        timestamp: timestamp,
    };

    myInput = {
      attribute: myAttributeInput,
      textColor: "",
      backgroundColor: "",
      size: 1,
      font: "",
      widgetCss: "",
    };
    let element = React.createElement(AlarmIndicator.component, {
      id: 1,
      mode: "library",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: myInput,
    });

    let wrapper = mount(<Provider store={store}>{element}</Provider>);
    expect(wrapper.find("#AlarmIndicatorLib").exists()).toBe(true);
  });

});
