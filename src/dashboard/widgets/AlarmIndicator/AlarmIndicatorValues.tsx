import React from "react";
import { IRootState } from "../../../shared/state/reducers/rootReducer";
import { useSelector } from "react-redux";
import "../Tabular/Tabular.css";
import {
    getAttributeLastValueFromState,
    getAttributeLastQualityFromState,
    getAttributeLastTimeStampFromState,
} from "../../../shared/utils/getLastValueHelper";
import MemoizedGetSvgIcon from "./SvgIcons";
import { ALARM_PRIORITY, ALARM_STATUS_NORM, alarmInfoAttribute } from "./AlarmConst";

interface Props {
    attributeName: string;
    deviceName: string;
    enumlabels: string[];
    mode: string;
}

interface Alarm {
    tag: string | null;
    state: string | null;
    priority: string | null;
    time: string | null;
    formula: string | null;
    message: string | null;
}

const AlarmIndicatorValues: React.FC<Props> = ({
    attributeName,
    deviceName,
    enumlabels,
    mode
}) => {

    const value = useSelector((state: IRootState) => {
        return getAttributeLastValueFromState(
            state.messages,
            deviceName,
            attributeName
        );
    });

    const alarmState = useSelector((state: IRootState) => {
        const values = getAttributeLastValueFromState(
            state.messages,
            deviceName,
            alarmInfoAttribute
        ) as string[];
        if(Array.isArray(values)){
            return values?.map(entry => {
                const entryObj: Partial<Alarm> = {}; // Use Partial<Alarm> to allow incremental building
                entry.split(";").forEach(part => {
                    const [key, ...valParts] = part.split("="); // Split at first "=" only
                    const val = valParts.join("="); // Join remaining parts in case value contains "="
                    entryObj[key.trim() as keyof Alarm] = val ? val.replace(/(^"|"$)/g, '').trim() : null; // Remove quotes around values
                });
                return entryObj as Alarm; // Assert the object to be of type Alarm
            });
        }
        else return undefined;
    });

    const timestamp = useSelector((state: IRootState) => {
        return getAttributeLastTimeStampFromState(
            state.messages,
            deviceName,
            attributeName
        )?.toString();
    });

    const quality = useSelector((state: IRootState) => {
        return getAttributeLastQualityFromState(
            state.messages,
            deviceName,
            attributeName
        )?.toString();
    });
    const enumValue = enumlabels?.[value];
    const priority = alarmState?.filter(alarm => alarm.tag === attributeName)?.[0]?.['priority'] || "";
    const title = (ALARM_PRIORITY[priority]) + " priority: " + enumlabels?.[value] + " - " + timestamp + " - " + quality;

    return (
        <div
            id="AlarmIndicatorValues"
            className="justify-content-left"
            title={title}
        >
            {enumValue !== ALARM_STATUS_NORM &&
                <MemoizedGetSvgIcon priority={priority} enumValue={enumValue} mode={mode}/>
            }
        </div>
    );
};

export default AlarmIndicatorValues;
