
export const alarmInfoAttribute = "alarmsummary";

export const LOG_ALARM = "log",
    FAULT_ALARM = "fault",
    WARNING_ALARM = "warning",

    ALARM_PRIORITY = {
        [LOG_ALARM]: "Low",
        [WARNING_ALARM]: "Medium",
        [FAULT_ALARM]: "High",
    },
    
    ALARM_STATUS_NORM = "NORM",
    ALARM_STATUS_UNACK = "UNACK",
    ALARM_STATUS_RTNUN = "RTNUN",
    ALARM_STATUS_SHLVD = "SHLVD",
    ALARM_ENUM_LABELS = [
        "NORM",
        "UNACK",
        "ACKED",
        "RTNUN",
        "SHLVD",
        "DSUPR",
        "OOSRV",
        "ERROR"
    ]
  

