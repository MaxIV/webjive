import { enrichedInputs } from "./enrichment";
import {
  InputDefinitionMapping,
  InputMapping,
  DeviceInput,
  AttributeInput,
  CommandInput,
} from "../types";

const inputs: InputMapping = {
  device: "device1",
  attribute: {
    device: "device2",
    attribute: "attribute2",
  },
  command: {
    device: "device3",
    command: "command3",
  },
};

const inputDefinitions: InputDefinitionMapping = {
  device: {
    type: "device",
  },
  attribute: {
    type: "attribute",
  },
  command: {
    type: "command",
  },
};
type enrichedInput = {
  device?: DeviceInput;
  attribute?: AttributeInput;
  command?: CommandInput;
};

test("device metadata", () => {
  const context = {
    deviceMetadataLookup() {
      return {
        alias: "an alias",
      };
    },
  };

  const spy = jest.spyOn(context, "deviceMetadataLookup");
  const result: enrichedInput = enrichedInputs(
    inputs,
    inputDefinitions,
    context
  );

  if (result.device !== undefined) {
    expect(result.device.alias).toBe("an alias");
  }
  expect(spy).toHaveBeenCalledWith("device1");
});

test("attribute values", () => {
  const context = {
    attributeValuesLookup() {
      return {
        value: "a value",
        writeValue: "a write value",
        timestamp: 12345,
      };
    },
  };
  const spy = jest.spyOn(context, "attributeValuesLookup");
  const result: enrichedInput = enrichedInputs(
    inputs,
    inputDefinitions,
    context
  );

  expect(spy).toHaveBeenCalledWith("device2/attribute2");
  if (result.attribute !== undefined) {
    expect(result.attribute.value).toBe("a value");
    expect(result.attribute.writeValue).toBe("a write value");
    expect(result.attribute.timestamp).toBe(12345);
  }
});

test("attribute write", async () => {
  const context = {
    async onWrite() {
      return;
    },
  };

  const spy = jest.spyOn(context, "onWrite");
  const result: enrichedInput = enrichedInputs(
    inputs,
    inputDefinitions,
    context
  );

  if (result.attribute !== undefined) {
    await result.attribute.write("a write value");
  }
  expect(spy).toHaveBeenCalledWith("device2", "attribute2", "a write value");
});

test("command execution", async () => {
  const context = {
    async onExecute() {
      return;
    },
  };

  const spy = jest.spyOn(context, "onExecute");
  const result: enrichedInput = enrichedInputs(
    inputs,
    inputDefinitions,
    context
  );

  const argin = Math.random();

  if (result.command !== undefined) {
    await result.command.execute(argin);
  }
  expect(spy).toHaveBeenCalledWith("device3", "command3", argin);
});

test("device input by default resolves to { name: <device name> }", () => {
  const result: enrichedInput = enrichedInputs(inputs, inputDefinitions);
  if (result.device !== undefined) {
    expect(result.device).toEqual({ name: "device1" });
  }
});

test("attribute.isNumeric by default resolves to false", () => {
  const result: enrichedInput = enrichedInputs(inputs, inputDefinitions);
  if (result.attribute !== undefined) {
    expect(result.attribute.isNumeric).toBe(false);
  }
});

test("attribute.dataType by default resolves to undefined", () => {
  const result: enrichedInput = enrichedInputs(inputs, inputDefinitions);
  if (result.attribute !== undefined) {
    expect(result.attribute.dataType).toBe(undefined);
  }
});

test("attribute.dataFormat by default resolves to undefined", () => {
  const result: enrichedInput = enrichedInputs(inputs, inputDefinitions);
  if (result.attribute !== undefined) {
    expect(result.attribute.dataFormat).toBe(undefined);
  }
});
