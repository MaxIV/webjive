import React from "react";
import TooltipIconWithModal from "./TooltipIconWithModal";
import { screen, render, fireEvent } from "@testing-library/react";
import '@testing-library/jest-dom';

describe("TooltipIconWithModal", () => {

  const headerTxt = "SVG Validation Result"
  test("renders the tooltip icon", () => {
    render(<TooltipIconWithModal />);
    const tooltipIcon = screen.getByText("i");
    expect(tooltipIcon).toBeInTheDocument()
  })

  test("check text present in modal", () => {
    render(<TooltipIconWithModal />);
    const tooltipIcon = screen.getByText("i");
    fireEvent.click(tooltipIcon);
  
    const modalHeaderText = screen.getByText(headerTxt);
    expect(modalHeaderText).toBeInTheDocument();
  })

  test("Close the modal when cicking outside of it", () => {
    render(<TooltipIconWithModal />);
    const tooltipIcon = screen.getByText("i");
    fireEvent.click(tooltipIcon);

    const modalOverlay = screen.getByText(headerTxt).parentElement;
    fireEvent.click(modalOverlay);
  })

  test("Close modal after close btn", () => {
    const msgs = [
      {
          "rule": {
              "type": "layer",
              "model": "sys/tg_test/1/short_scalarr",
              "layer": "path21",
              "condition": "value>21 && value<90"
          },
          "error": "Layer \"path21\" not found in SVG. Expected attribute: inkscape:label=\"path21\""
      },
      {
          "item": {
              "type": "layer",
              "model": "sys/tg_test/1/short_scalarr",
              "layer": "path21",
              "condition": "value>21 && value<90"
          },
          "error": "Attribute 'sys/tg_test/1/short_scalarr' not present."
      }
    ]
    render(<TooltipIconWithModal messages={msgs} />);
    const tooltipIcon = screen.getByText("i");
    fireEvent.click(tooltipIcon);
    const msg = screen.getByText("Attribute 'sys/tg_test/1/short_scalarr' not present.");
    expect(msg).toBeInTheDocument()

    const closeBtn = screen.getByText("Close");
    fireEvent.click(closeBtn);
  
    const headerText = screen.queryByText(headerTxt);
    expect(headerText).not.toBeInTheDocument();

  })
})