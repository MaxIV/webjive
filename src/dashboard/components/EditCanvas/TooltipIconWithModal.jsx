import React, { useState } from "react";
import "./TooltipIconWithModal.css";
import { Button } from "react-bootstrap";
import Modal from "../../../shared/modal/components/Modal/Modal";

const TooltipIconWithModal = ({ messages }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const msgArr = messages?.map(msg => {
    return {
      type: "warning",
      msg: msg
    }
  })

  const toggleModal = () => {
    setIsModalOpen(!isModalOpen);
  };

  return (
    <div>
      <div className="tooltip-square-icon" onClick={toggleModal}>
        i
      </div>

      {isModalOpen && (
        <Modal title="SVG Validation Result" size="md">
            <Modal.Body>
                {msgArr?.length === 0 &&
                    <div>There are no messages</div>
                }
                {msgArr?.length > 0 && msgArr.map((row, key) => {
                    return <div key={key} className={`alert alert-${row.type}`}>
                            {row?.msg?.error}
                          </div>
                })}
            </Modal.Body>

            <Modal.Footer>
                <Button variant="primary" onClick={toggleModal}>
                    Close
                </Button>
            </Modal.Footer>
        </Modal>
    )}
    </div>
  );
};

export default TooltipIconWithModal;
