import React, { Component } from "react";
import { connect } from "react-redux";

import RunnableCanvas, {
  DispatchProps,
  RunnableProps,
} from "../../components/RunCanvas/RunnableCanvas";

import {
  getUsername,
  getIsLoggedIn,
} from "../../../shared/user/state/selectors";
import { getTangoDBName } from "../../../shared/state/selectors/database";
import {
  getSelectedDashboard,
  getDashboards,
  getCurrentDashoardVariables,
} from "../../../shared/state/selectors";
import { getWidgets } from "../../../shared/state/selectors";
import { saveNotification } from "../../../shared/user/state/actionCreators";
import { Notification } from "../../../shared/notifications/notifications";
import { IRootState } from "../../../shared/state/reducers/rootReducer";
import { getDeviceNames } from "../../../shared/state/selectors/deviceList";
import "./RunCanvas.css";
import { variablesClassAndDevicesToLowerCase } from "../../../shared/utils/DashboardVariables";

export const HISTORY_LIMIT = 1000;
/**
 * @class RunCanvas is the main component which renders the current selected dashboard.
 *
 * It gets all its state from redux store, so it's in sync with the Dashboard application.
 *
 * All the rendering is delegated to a RunnableCanvas in order to share the logic with the CraftCanvas.
 *
 */

type Props = RunnableProps & DispatchProps;
class RunCanvas extends Component<Props> {
  public constructor(props: Props) {
    super(props);
  }

  public render() {
    let {
      username,
      isLoggedIn,
      widgets,
      tangoDB,
      devices,
      selectedDashboard,
      dashboards,
      dashboardVariables,
      onSaveNotification,
    } = this.props;

    //This mantains compatibility on old dashboards that might use devices with uppercase names
    const lowercaseVariables = variablesClassAndDevicesToLowerCase(dashboardVariables);

    return (
      <div className="RunCanvas">
        <RunnableCanvas
          username={username}
          isLoggedIn={isLoggedIn}
          widgets={widgets}
          tangoDB={tangoDB}
          devices={devices}
          selectedDashboard={selectedDashboard}
          dashboards={dashboards}
          dashboardVariables={lowercaseVariables}
          onSaveNotification={onSaveNotification}
        />
      </div>
    );
  }
}

function mapStateToProps(state: IRootState): RunnableProps {
  return {
    username: getUsername(state),
    isLoggedIn: getIsLoggedIn(state),
    widgets: getWidgets(state),
    tangoDB: getTangoDBName(state),
    dashboards: getDashboards(state),
    devices: getDeviceNames(state),
    selectedDashboard: getSelectedDashboard(state),
    dashboardVariables: getCurrentDashoardVariables(state),
  };
}

function mapDispatchToProps(dispatch): DispatchProps {
  return {
    onSaveNotification: (notification: Notification, username: string) =>
      dispatch(saveNotification(notification, username)),
  };
}

export default connect<RunnableProps, DispatchProps, Props, IRootState>(
  mapStateToProps,
  mapDispatchToProps
)(RunCanvas);
