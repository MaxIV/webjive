import React from "react";

export default function ErrorWidget({ error }) {
  return (
    <div
      style={{
        backgroundColor: "pink",
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        fontSize: "small",
      }}
    >
      <span className="fa fa-exclamation-triangle" />
      ️️ {String(error)}
    </div>
  );
}
