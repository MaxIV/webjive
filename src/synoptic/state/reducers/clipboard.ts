import { SynopticAction } from "../actions";

export interface ClipboardState {
  pasteCounter: number;
}

const initialState: ClipboardState = {
  pasteCounter: 0,
};

export default function clipboard(
  state: ClipboardState = initialState,
  action: SynopticAction
): ClipboardState {
  switch (action.type) {
    default:
      return state;
  }
}
